package frontend;

import java.awt.*;
import javax.swing.*;


import frontend.components.*;
import frontend.pages.*;
import shared.*;

public class StudentGUI extends PageNavigator {
	private static String username;
	private static String password;
	private boolean isProfessor = false;
	private Client client;
	private int response;
	String[] coursearray;

	public StudentGUI(String username, String password) {
		super();

		this.username = username;
		this.password = password;
		client = new Client("localhost", 9090);
		response = client.checkCredentials(username, password);
		if (response == -1) { // check if valid user and pass
			JOptionPane.showMessageDialog(null, "Invalid User and Pass", "Error", JOptionPane.ERROR_MESSAGE);
			isProfessor = false;
		} else if (response == 0) {
			isProfessor = false;
		}
		else {
			isProfessor = true;
		}
		if (isProfessor) {
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

			JFrame frame = new JFrame("Professor GUI");
			frame.setSize(500,600);
			frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);



			this.pageHolder.add(this.homePage,"home");

//            this.cardLayout.show(this.pageHolder, "home");

			this.homePage.setStudent(this);
			this.coursePage.setStudent(this);
			this.assignmentPage.setStudent(this);
			this.gradePage.setStudent(this);
			this.submissionPage.setStudent(this);


			this.pageHolder.add(this.homePage,"home");
			this.pageHolder.add(this.coursePage,"course");
			this.pageHolder.add(this.assignmentPage,"assignment");
			this.pageHolder.add(this.gradePage,"grade");
			this.pageHolder.add(this.submissionPage,"submission");

			this.cardLayout.show(this.pageHolder, "home");
			frame.getContentPane().add("Center", this.pageHolder);
			frame.setVisible(true);
		}
	}

	public int getResponse() {
		return response;
	}

	public Client getClient(){
		return this.client;
	}

	public String getUsername() {
		return username;
	}

	public static void main(String[] args) {
		StudentGUI studentGUI = new StudentGUI(username, password);
	}


}
