/**
 * 
 */
package frontend.components;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

/**
 * @author ArjunS
 *
 */
public class EmailHelper {
	// String recipientEmail, senderEmail, content, sub, senderPass;
	final String GMAIL = "smtp.gmail.com";
	final String YMAIL = "smtp.yahoo.com";
	final String SSLPORT465 = "465";
	final String PORT587 = "587";

	/**
	 * @param recipientEmail
	 *            : recipientEmail
	 * @param senderEmail
	 *            : senderEmail
	 * @param content
	 *            : content
	 * @param subject
	 *            : subject
	 * @param senderPass
	 *            : senderPass
	 * @wbp.parser.entryPoint
	 */
	public EmailHelper(String recipientEmail, String senderEmail, String content, String subject, String senderPass) {
		Properties props = new Properties();
		props.put("mail.smtp.host", GMAIL);
		props.put("mail.smtp.socketFactory.port", SSLPORT465);
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", SSLPORT465);

		// props.put("mail.smtp.starttls.enable", "true");
		// props.put("mail.smtp.auth", "true");
		// props.put("mail.smtp.host", GMAIL);
		// props.put("mail.smtp.port", PORT587);

		// SecurityManager security = System.getSecurityManager();
		boolean success = false;
		try {
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(senderEmail, senderPass);
				}
			};

			Session session = Session.getInstance(props, auth);
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(senderEmail));
			msg.setSubject(subject);
			msg.setText(content);
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(recipientEmail));

			Transport.send(msg);

			success = true;
		} catch (Exception e) {
			success = false;
			System.out.println(e);
			e.printStackTrace();
		} finally {
			if (success) {
				System.out.println("EMAIL SENT");
				JOptionPane.showMessageDialog(null, senderEmail + " you have successfully sent the email ",
						"Sending Successfull", JOptionPane.INFORMATION_MESSAGE);
			}
		}

	}

}
