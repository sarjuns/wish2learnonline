package frontend.components;

import java.awt.*;

import javax.swing.*;

import frontend.pages.*;

public class PageNavigator extends JFrame {
	public JPanel pageHolder;
	public CardLayout cardLayout;
	public HomePage homePage;
	public CoursePage coursePage;
	public AssignmentPage assignmentPage;
	public GradePage gradePage;
	public SubmissionPage submissionPage;
	public EnrollmentPage enrollmentPage;
	public EmailWindow emailWindow;
	public Page page;
	
	public PageNavigator() {
		this.cardLayout = new CardLayout();
		this.pageHolder = new JPanel(cardLayout);
		this.coursePage = new CoursePage();
		this.assignmentPage = new AssignmentPage();
		this.submissionPage = new SubmissionPage();
		this.homePage = new HomePage();
		this.gradePage = new GradePage();
		this.enrollmentPage = new EnrollmentPage();
		this.emailWindow = new EmailWindow();
	}
	
	/*public void showPage(String page) {
		this.show(pageHolder,page);
	}*/
	public void addPage(JPanel page, String name) {
		this.add(page, name);
	}

	public void removePage(String name) {
		
	}
	public JPanel searchPage(String name) {
		return pageHolder;
		
	}
}
