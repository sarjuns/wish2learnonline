package frontend;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class LoginWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8777310078433832291L;
	ProfessorGUI professorGUI;
	StudentGUI studentGUI;

	private JPanel pnlLogin = new JPanel(new GridBagLayout());
	private GridBagConstraints constraints = new GridBagConstraints();

	private JLabel lblUsername = new JLabel("Username:");
	private JTextField fldUsername = new JTextField(15);

	private JLabel lblPassword = new JLabel("Password:");
	private JPasswordField fldPassword = new JPasswordField(15);

	private JButton bttnLogin = new JButton("Login");

	protected LoginWindow() {
		super("Wish2Learn");
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize(300, 200);
		this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		pnlLogin.setBorder(
				BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Central Authentication Service"));
		constraints.anchor = GridBagConstraints.WEST;
		constraints.insets = new Insets(10, 10, 10, 10);

		// Username
		constraints.gridx = 0;
		constraints.gridy = 0;
		pnlLogin.add(lblUsername, constraints);
		constraints.gridx = 1;
		constraints.gridy = 0;
		pnlLogin.add(fldUsername, constraints);

		// Password
		constraints.gridx = 0;
		constraints.gridy = 1;
		pnlLogin.add(lblPassword, constraints);
		constraints.gridx = 1;
		constraints.gridy = 1;
		fldPassword.setEchoChar('*');
		pnlLogin.add(fldPassword, constraints);

		// Login
		constraints.anchor = GridBagConstraints.EAST;
		constraints.gridx = 1;
		constraints.gridy = 4;
		constraints.insets = new Insets(10, 10, 10, 10);
		pnlLogin.add(bttnLogin, constraints);
		bttnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == bttnLogin)
					login();
			}
		});

		this.getContentPane().add(pnlLogin);
		this.pack();
	}

	public void login() {
		String username = fldUsername.getText();
		String password = String.valueOf(fldPassword.getPassword());
		professorGUI = new ProfessorGUI(username, password);
		// studentGUI= new StudentGUI(username,password);
		if (professorGUI.getResponse() != -1) {
			this.setVisible(false);
		}
		System.out.println(username + " " + password);
	}

	public static void main(String[] args) {
		LoginWindow loginWindow = new LoginWindow();
	}
}
