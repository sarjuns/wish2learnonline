package frontend;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ProfGUItest extends JFrame{
	
	private JPanel pnlTop= new JPanel(new FlowLayout(FlowLayout.RIGHT));
	
	private JButton bttnCreate = new JButton("Create Course");
	private JButton bttnBrowse = new JButton("Browse Courses");
	
	private CardLayout cardLayout = new CardLayout();
	private JPanel pnlContainer = new JPanel(cardLayout);
	private JPanel pnlHome = new JPanel(new GridBagLayout());
	private JPanel pnlCreate = new JPanel(new GridBagLayout());
	private JPanel pnlBrowse = new JPanel(new BorderLayout());
	private GridBagConstraints constraints = new GridBagConstraints();

	//for pnlCreate:
	private JLabel lblCourseNumber = new JLabel("Course Numeber:");
	private JTextField fldCourseNumber = new JTextField(20);
	private JLabel lblCourseName = new JLabel("Course Name:");
	private JTextField fldCourseName = new JTextField(20);
	private JLabel lblVisibility = new JLabel("Student Visibility:");
	private ButtonGroup grpVisibility = new ButtonGroup(); //select just 1 option
	private JRadioButton bttnActive = new JRadioButton("Active");
	private JRadioButton bttnInactive = new JRadioButton("Inactive");
	private JButton bttnConfirmCreate = new JButton("Create");
	
	//for pnlBrowse:
	private JComboBox boxCourseList = new JComboBox();
	private JPanel pnlCourseInfo = new JPanel(new GridLayout(4,1));
	private JButton bttnSearchStud = new JButton("Student Search");
	private JButton bttnUploadAss = new JButton("Upload Assignment");
	private JButton bttnSendEmail = new JButton("Create Email");
	private JButton bttnDropbox = new JButton("Assignment Dropbox");
	
	//for searchStud Panel
	private JPanel pnlSearchStud = new JPanel(null);
	private JLabel lblSearchType = new JLabel("Search Student by:");
	private String[] studentParam = { "", "Student ID", "Student Last Name"};
	private JComboBox studentTypeList = new JComboBox(studentParam);
	private JLabel lblSearchParam = new JLabel("Enter the search parameter below:");
	private JTextField fldSearchParam = new JTextField(20);
	private JButton bttnConfirmSearch = new JButton("Search Student");
	private JLabel lblSearchResult = new JLabel("Search Result:");
	private JTextField fldSearchResult = new JTextField(20);
	private JScrollPane scrollResult = new JScrollPane(fldSearchResult);
	
	protected ProfGUItest() {
		JFrame frame = new JFrame("Wish2Learn: Professor");
		this.setVisible(true);
	    this.setSize(600,500);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
		pnlTop.setBorder(BorderFactory.createEtchedBorder());    
	    pnlTop.add(bttnCreate);
	    pnlTop.add(bttnBrowse);
	    
	    this.getContentPane().add("North",pnlTop);
	    
	    //Placeholder
	    pnlHome.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "HOME"));
	    pnlContainer.add(pnlHome,"home");
	    
	    
	    //Create Panel
	    pnlCreate.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Creating a  new course:"));
	    pnlContainer.add(pnlCreate,"create");
	    constraints.anchor = GridBagConstraints.WEST;
		constraints.insets = new Insets(10, 10, 10, 10);
		
	    constraints.gridx = 0;
		constraints.gridy = 0;
		pnlCreate.add(lblCourseNumber,constraints);
		constraints.gridx = 1;
		constraints.gridy = 0;
		pnlCreate.add(fldCourseNumber,constraints);
	    
		constraints.gridx = 0;
		constraints.gridy = 1;
		pnlCreate.add(lblCourseName,constraints);
		constraints.gridx = 1;
		constraints.gridy = 1;
		pnlCreate.add(fldCourseName,constraints);
		
		
		constraints.gridx = 0;
		constraints.gridy = 2;
		pnlCreate.add(lblVisibility,constraints);
		constraints.gridx = 1;
		constraints.gridy = 2;
		constraints.insets = new Insets(0, 0, 0, 0);
		pnlCreate.add(bttnActive,constraints);
		constraints.gridx = 1;
		constraints.gridy = 3;
		pnlCreate.add(bttnInactive,constraints);
		
		constraints.insets = new Insets(10,10,10,10);
		constraints.anchor = GridBagConstraints.EAST;
		constraints.gridx = 1;
		constraints.gridy = 4;
		pnlCreate.add(bttnConfirmCreate,constraints);
	    
	    //Browse Panel
	    pnlBrowse.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Browsing courses:"));
	    pnlContainer.add(pnlBrowse,"browse");
	    pnlBrowse.add("North",boxCourseList);
	    
	    
	    //Course Info Panel
	    //SearchStud Panel
	    lblSearchType.setBounds(300, 400, 60, 10);
	    pnlSearchStud.add(lblSearchType);
	    lblSearchType.setBounds(300, 425, 60, 10);
	    pnlSearchStud.add(studentTypeList);
	    lblSearchType.setBounds(300, 450, 60, 10);
	    pnlSearchStud.add(lblSearchParam);
	    lblSearchType.setBounds(300, 475, 60, 10);
	    pnlSearchStud.add(fldSearchParam);
	    lblSearchType.setBounds(300, 500, 60, 20);
	    pnlSearchStud.add(bttnConfirmSearch);
	    lblSearchType.setBounds(300, 525, 60, 20);
	    pnlSearchStud.add(lblSearchResult);
	    lblSearchType.setBounds(300, 550, 60, 20);
	    pnlSearchStud.add(scrollResult);
	    pnlBrowse.add("Center",pnlSearchStud);
	    
	    
	    pnlCourseInfo.add(bttnSearchStud,constraints);
	    bttnSearchStud.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
			}
		});
	    pnlCourseInfo.add(bttnUploadAss,constraints);
	    bttnSearchStud.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
			}
		});
	    pnlCourseInfo.add(bttnSendEmail,constraints);
	    bttnSearchStud.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
			}
		});
	    pnlCourseInfo.add(bttnDropbox,constraints);
	    bttnSearchStud.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
			}
		});
	    pnlBrowse.add("West",pnlCourseInfo);
	    
	    //Create button
	    bttnCreate.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					cardLayout.show(pnlContainer,"create");
				}
			});
	    
	    //Browse button
	    bttnBrowse.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				cardLayout.show(pnlContainer,"browse");
			}
		});

	    
	    this.getContentPane().add("Center",pnlContainer);
	    cardLayout.show(pnlContainer, "home");
	}
	
	public static void main(String[] args){
		ProfGUItest profGUI = new ProfGUItest();
	}
}
