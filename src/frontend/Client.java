package frontend;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

import shared.*;
// Assume all necessary file are imported
public class Client implements CommandConstants{

    private PrintWriter socketOutput;
    private Socket aSocket;
    private BufferedReader stdIn;
    private BufferedReader socketInput;

    private InputStream is;
    private ObjectInputStream objin;
    private OutputStream os;
    private ObjectOutputStream objout;

    public Client(String serverName, int portNumber) {
        try {
            aSocket = new Socket(serverName, portNumber);
            stdIn = new BufferedReader(new InputStreamReader(System.in));
            socketInput = new BufferedReader(new InputStreamReader(aSocket.getInputStream()));
            socketOutput = new PrintWriter((aSocket.getOutputStream()), true);
            setOutputStream();
            setInputStream();
        } catch (IOException e) {
            System.err.println(e.getStackTrace());
        }
    }

    public File getFile(String path){
        //TODO
        return null;
    }

    public boolean setCourseVisibility(int courseID, boolean cond){
        int i = 0;
        i = (cond)? 1 : 0;
        socketOutput.println(String.valueOf(courseID) + " " + String.valueOf(i) + " " + COMMANDS[5]);
        return true;
    }

    //TODO
    /*
        get students
     */
//    public ArrayList<Student> getStudents(String id, String column){
//        socketOutput.println(id + " " + ln + " " + COMMANDS[4]);
//
//        try {
//            response = socketInput.readLine();
//        } catch (IOException e){
//            System.out.println("Error in checkCredentials");
//            e.printStackTrace();
//        }
//        return null;
//    }

    public User getStudent(String id){
        socketOutput.println(id+" " + COMMANDS[4]);
        String response;
        User student = null;
        try {
            response = socketInput.readLine();
            while (!response.equals("Done sending user to client")) {
                if (response.equals("Sending user to client")) {
                    System.out.println("in getStudent");
                    student = (User)objin.readObject();
                }
                response = socketInput.readLine();
            }
        } catch (ClassNotFoundException e){

        }
        catch (IOException e){
            e.printStackTrace();
            System.out.println("Error in getstudent");
        }
        return student;
    }

    /*
    initial check credantial
     */
    public int checkCredentials(String user, String pass){
        String response = "";
        socketOutput.println(user + " " + pass + " " + COMMANDS[0]);

        try {
            response = socketInput.readLine();
            return Integer.parseInt(response);
        } catch (IOException e){
            System.out.println("Error in checkCredentials");
            e.printStackTrace();
        }

        return -1;
    }

    public boolean insertCourse(Course c){
        socketOutput.println(COMMANDS[3]);
        try {
            objout.writeObject(c);
            objout.flush();
            String response = socketInput.readLine();
            System.out.println(response);
            if(response.equals("success in addCourse"));
                return true;
        } catch (IOException e){
            e.printStackTrace();
            System.out.println("Error sending course thru socket.");
        }
        return false;
    }

    public void uploadAssignment(File a){
        String response = "";
        long length = a.length();
        byte[] content = new byte[(int) length]; // Converting Long to Int
        try{
            FileInputStream fis = new FileInputStream(a);
            BufferedInputStream bos = new BufferedInputStream(fis);
            bos.read(content, 0, (int)length);

            socketOutput.println(COMMANDS[2]);
            objout.writeObject(content);
            objout.flush();
            socketOutput.flush();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }

    }

    public ArrayList<Course> getCourses(String id){
        String response = "";
//        String[] arr = null;
        ArrayList<Course> arr = null;
        socketOutput.println(id + " " + COMMANDS[1]);
        try {
            response = socketInput.readLine();
            while (!response.equals("Done sending courses to client")) {
                if (response.equals("Sending courses to client")) {
                    System.out.println("in getCourses");
                    arr = getCourseArr();
                }
                response = socketInput.readLine();
            }
        } catch (IOException e){
            e.printStackTrace();
            System.out.println("Error in getCourses");
        }
        return arr;
    }

    public ArrayList<Course> getCourseArr(){
        ArrayList<Course> in = null;
        try {
            in = (ArrayList<Course>) objin.readObject();
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.err.println("Class not found exception caught.");
        }
        catch(IOException e){
            System.err.println("Error in getCourseArr");
        }
        return in;
    }

    public void setOutputStream() {
        try {
            os = aSocket.getOutputStream();
            objout = new ObjectOutputStream(os);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error setting output stream.");
        }
    }

    public void setInputStream() {
        try {
            is = aSocket.getInputStream();
            objin = new ObjectInputStream(is);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error setting input stream");
        }
    }

//    public Message getMsg() throws IOException{
//        Message in = null;
//        try {
//            in = (Message) objin.readObject();
//        }catch (ClassNotFoundException e) {
//            e.printStackTrace();
//            System.err.println("Class not found exception caught.");
//        }catch(Exception e){
//            System.err.println("Cant get message");
//        }
//        return in;
//    }

//    public void sendMessageToServer(Message in){
//        socketOutput.println("Sending message to server");
//        try {
//            objout.writeObject(in);
//        } catch (IOException e){
//            e.printStackTrace();
//            System.out.println("Error sending message.");
//        }
//        socketOutput.println("Done sending message to server");
//    }

    public static void main(String[] args) throws IOException {
        Client aClient = new Client("localhost", 9090);
    }
}
