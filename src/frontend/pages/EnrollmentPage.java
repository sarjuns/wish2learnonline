package frontend.pages;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;

import frontend.ProfessorGUI;
import frontend.StudentGUI;
import shared.*;

public class EnrollmentPage extends Page{
	Course course;
	ArrayList<Student> enrolledStudentList;

	public EnrollmentPage(){
        setLayout(null);

        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Student Information"));
        panel.setBounds(34, 34, 416, 310);
        add(panel);
        panel.setLayout(null);

        JLabel lblNewLabel = new JLabel("First Name:");
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblNewLabel.setBounds(36, 54, 78, 14);
        panel.add(lblNewLabel);

        JLabel lblNewLabel_1 = new JLabel("Last Name:");
        lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblNewLabel_1.setBounds(36, 103, 78, 14);
        panel.add(lblNewLabel_1);

        JLabel lblNewLabel_2 = new JLabel("Student Email:");
        lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblNewLabel_2.setBounds(36, 152, 97, 14);
        panel.add(lblNewLabel_2);

        JLabel lblNewLabel_3 = new JLabel("Student ID:");
        lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblNewLabel_3.setBounds(36, 203, 78, 14);
        panel.add(lblNewLabel_3);

        JLabel lblNewLabel_4 = new JLabel("Enrollment Status:");
        lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblNewLabel_4.setBounds(36, 252, 111, 14);
        panel.add(lblNewLabel_4);

        textField = new JTextField();
        textField.setBounds(186, 100, 183, 20);
        panel.add(textField);
        textField.setColumns(10);

        textField1 = new JTextField();
        textField1.setBounds(186, 51, 183, 20);
        panel.add(textField1);
        textField1.setColumns(10);

        textField2 = new JTextField();
        textField2.setBounds(186, 249, 183, 20);
        panel.add(textField2);
        textField2.setColumns(10);

        textField3 = new JTextField();
        textField3.setBounds(186, 149, 183, 20);
        panel.add(textField3);
        textField3.setColumns(10);

        textField4 = new JTextField();
        textField4.setBounds(186, 200, 183, 20);
        panel.add(textField4);
        textField4.setColumns(10);

        JButton btnBack = new JButton("< Back");
        //btnBack.setFont(new Font("Tahoma", Font.BOLD, 11));
        btnBack.setBounds(15, 525, 89, 23);
        btnBack.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                professor.cardLayout.show(professor.pageHolder, "course");
            }
        });
        add(btnBack);

        JButton btnUnenroll = new JButton("Unenroll Student");
        //btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 11));
        btnUnenroll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
            }
        });
        btnUnenroll.setBounds(253, 383, 130, 23);
        add(btnUnenroll);

        JButton btnEnroll = new JButton("Enroll Student");
        btnEnroll.setFont(new Font("Tahoma", Font.BOLD, 11));
        btnEnroll.setBounds(125, 383, 118, 23);
        add(btnEnroll);
	}
    public void setProfessor(ProfessorGUI p){
        this.professor = p;
    }


}
