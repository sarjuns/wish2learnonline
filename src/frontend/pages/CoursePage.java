package frontend.pages;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.*;

import frontend.ProfessorGUI;
import frontend.StudentGUI;
import shared.*;

public class CoursePage extends Page {
	Course course;


	public CoursePage() {
	    if(isProfessor == true) {
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "COURSE"));
            setLayout(null);

            JButton btnActivate = new JButton("Activate Course");
            btnActivate.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    System.out.println("pressed active");
                    boolean check = professor.getClient().setCourseVisibility(courseID, true);
                    if (check) {
                        JOptionPane.showMessageDialog(null, "Set course: " + courseID + " to active", "Success", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Could not change visiblity", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }

            });
            //btnActivate.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnActivate.setBounds(111, 11, 137, 23);
            add(btnActivate);

            JButton btnDeactivate = new JButton("Deactivate Course");
            btnDeactivate.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    System.out.println("pressed deactive");
                    boolean check = professor.getClient().setCourseVisibility(courseID, false);
                    if (check) {
                        JOptionPane.showMessageDialog(null, "Set course: " + courseID + " to inactive", "Success", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Could not change visiblity", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }

            });
            //btnDeactivate.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnDeactivate.setBounds(261, 11, 150, 23);
            add(btnDeactivate);

            JLabel lblSearchStudentBy = new JLabel("Student Search:");
            //lblSearchStudentBy.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblSearchStudentBy.setBounds(10, 45, 103, 14);
            add(lblSearchStudentBy);

            textField = new JTextField();
            textField.setBounds(29, 70, 339, 20);
            add(textField);
            textField.setColumns(10);

            JRadioButton rdbtnStudentId = new JRadioButton("Student ID");
            rdbtnStudentId.setBounds(160, 41, 89, 23);
            add(rdbtnStudentId);

            JRadioButton rdbtnLastName = new JRadioButton("Last Name");
            rdbtnLastName.setBounds(259, 41, 89, 23);
            add(rdbtnLastName);

            radioButtGroup = new ButtonGroup();
            radioButtGroup.add(rdbtnStudentId);
            radioButtGroup.add(rdbtnLastName);

            JButton btnSearch = new JButton("Search");
            btnSearch.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    System.out.println("pressed search students courseID is:" + courseID);

                    ArrayList<Student> students = null;
                    User ns;
                    String param;

                    if (rdbtnStudentId.isSelected()) {
                        param = textField1.getText();
                        System.out.println("id param is: " + param);
                        ns = professor.getClient().getStudent(param);
                        System.out.println(ns.getFirstname());
                    } else if (rdbtnLastName.isSelected()) {
                        param = textField1.getText();
                        System.out.println("last name param is: " + param);
                        //					students = professor.getClient().getStudents(param);
                    }
                }
            });
            //btnSearch.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnSearch.setBounds(379, 69, 89, 23);
            add(btnSearch);

            JList list = new JList();
            list.setBounds(29, 126, 439, 228);
            add(list);

            JLabel lblEnrolledStudents = new JLabel("Enrolled Students:");
            //lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblEnrolledStudents.setBounds(10, 101, 103, 14);
            add(lblEnrolledStudents);

            JLabel lblCourseAssignments = new JLabel("Course Assignments:");
            //lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblCourseAssignments.setBounds(10, 360, 137, 14);
            add(lblCourseAssignments);

            JList list_1 = new JList();
            list_1.setBounds(29, 385, 439, 100);
            add(list_1);

            JButton btnBack = new JButton("< Back");
            btnBack.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    System.out.println("pressed back");
                    professor.cardLayout.show(professor.pageHolder, "home");
                }

            });
            //btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnBack.setBounds(15, 525, 89, 23);
            add(btnBack);

            JButton btnAddAssignment = new JButton("Add Assignment");
            //btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnAddAssignment.setBounds(180, 492, 137, 23);
            add(btnAddAssignment);

            JButton btnSendEmail = new JButton("Send Email >");
            //btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnSendEmail.setBounds(360, 525, 115, 23);
            add(btnSendEmail);
            btnSendEmail.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                	System.out.println("sending Email");
                	 professor.cardLayout.show(professor.pageHolder, "email");
                }    

            });
        }
        else{
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "COURSE"));
            setLayout(null);

            JLabel lblCourseName = new JLabel("Insert Course Name Here");
            lblCourseName.setHorizontalAlignment(SwingConstants.CENTER);
            //lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblCourseName.setBounds(170, 33, 154, 14);
            add(lblCourseName);

            JLabel lblCourseAssignments = new JLabel("Course Assignments:");
            //lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblCourseAssignments.setBounds(10, 101, 138, 14);
            add(lblCourseAssignments);

            JButton btnBack = new JButton("< Back");
            //btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnBack.setBounds(15, 525, 89, 23);
            add(btnBack);

            JList list = new JList();
            list.setBounds(29, 126, 439, 357);
            add(list);

            JButton btnSendEmail = new JButton("Send Email >");
            //btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnSendEmail.setBounds(360, 525, 115, 23);
            add(btnSendEmail);
        }
	    
	    



		/*panelTop = new JPanel(new BorderLayout());
		panelTop1 = new JPanel(new GridBagLayout());
		panelTop2 = new JPanel(new GridBagLayout());
		panelCentre = new JPanel(new BorderLayout());
		panelBottom = new JPanel(new BorderLayout());
		panelBottomStuff = new JPanel(new BorderLayout());
		panelBottomBack = new JPanel(new GridBagLayout());

		setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "COURSE"));
		setLayout(new BorderLayout());

		constraints = new GridBagConstraints();
		constraints.insets = new Insets(10,10,10,10);
		constraints.anchor = GridBagConstraints.CENTER;

		button1 = new JButton("Activate Course");
		button1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.out.println("pressed active");
                boolean check = professor.getClient().setCourseVisibility(courseID,true);
                if(check){
                    JOptionPane.showMessageDialog(null, "Set course: "+courseID+" to active", "Success", JOptionPane.INFORMATION_MESSAGE);
                }
                else{
                    JOptionPane.showMessageDialog(null, "Could not change visiblity", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }

        });

		button2 = new JButton("Deactivate Course");
		button2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.out.println("pressed deactive");
                boolean check = professor.getClient().setCourseVisibility(courseID,false);
                if(check){
                    JOptionPane.showMessageDialog(null, "Set course: "+courseID+" to inactive", "Success", JOptionPane.INFORMATION_MESSAGE);
                }
                else{
                    JOptionPane.showMessageDialog(null, "Could not change visiblity", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }

        });

		panelTop1.add(button1,constraints);
		panelTop1.add(button2,constraints);

		label1 = new JLabel("Search for a student:");
		textField1 = new JTextField(15);
		button3 = new JButton("Search");

		constraints.gridy = 0;
		panelTop2.add(label1,constraints);
		radioButt1 = new JRadioButton("Search by ID");
		radioButt2 = new JRadioButton("Search by Last Name");
		radioButtGroup = new ButtonGroup();
		radioButtGroup.add(radioButt1);
		radioButtGroup.add(radioButt2);

		button3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.out.println("pressed search students courseID is:" +courseID);

				ArrayList<Student> students = null;
				User ns;
				String param;

				if(radioButt1.isSelected()){
					param = textField1.getText();
					System.out.println("id param is: " + param);
					ns = professor.getClient().getStudent(param);
					System.out.println(ns.getFirstname());
				}
				else if(radioButt2.isSelected()){
					param = textField1.getText();
					System.out.println("last name param is: " + param);
//					students = professor.getClient().getStudents(param);
				}
			}
		});

		panelTop2.add(radioButt1,constraints);
		panelTop2.add(radioButt2,constraints);
		constraints.gridy = 1;
		constraints.gridwidth = 2;;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		panelTop2.add(textField1,constraints);
		constraints.gridwidth = 1;
		panelTop2.add(button3,constraints);

		panelTop.add("North",panelTop1);
		panelTop.add("Center",panelTop2);

		list1 = new JList(studentArray);
		scrollPane1 = new JScrollPane(list1);
		scrollPane1.setPreferredSize(new Dimension(40, 30));
		label2 = new JLabel("Students enrolled in course:");
		panelCentre.add("North",label2);
		panelCentre.add("Center",scrollPane1);



		list2 = new JList(assignmentArray);
		list2.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent mouseEvent) {
				professor.pageHolder.add(professor.assignmentPage,"assignment");
				JList theList = (JList) mouseEvent.getSource();
				if (mouseEvent.getClickCount() == 2) {
					int index = theList.locationToIndex(mouseEvent.getPoint());
					if (index >= 0) {
						Object o = theList.getModel().getElementAt(index);
						System.out.println("Double-clicked on: " + o.toString());
						professor.cardLayout.show(professor.pageHolder, "assignment");
					}
				}
			}
		});
		scrollPane1 = new JScrollPane(list2);
		label3 = new JLabel("Course Assignments:");
		button4 = new JButton("Add an Assignment");
		panelBottomStuff.add("North",label3);
		panelBottomStuff.add("Center",scrollPane1);
		panelBottomStuff.add("South",button4);

		button5 = new JButton("< Back");
		button5.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				professor.pageHolder.add(professor.homePage,"home");
				System.out.println("pressed back");
				professor.cardLayout.show(professor.pageHolder,"home");
			}

		});
		constraints.anchor = GridBagConstraints.WEST;
		panelBottomBack.add(button5, constraints);

		panelBottom.add("North",panelBottomStuff);
		panelBottom.add("West",panelBottomBack);

		add("North",panelTop);
		add("Center",panelCentre);
		add("South",panelBottom);

*/
	}

    public void setProfessor(ProfessorGUI p){
        this.professor = p;
    }
    public void setStudent(StudentGUI s){
        this.student = s;
    }
}
