package frontend.pages;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import javax.swing.*;

import frontend.ProfessorGUI;
import frontend.StudentGUI;
import shared.*;

public class AssignmentPage extends Page{
	Course course;
	Assignment assignment;

	public AssignmentPage() {

	    if(isProfessor == true) {
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "ASSIGNMENT"));
            setLayout(null);
            JButton btnActivate = new JButton("Activate Assignment");
            //btnActivate.setFont(new Font("Tahoma", Font.PLAIN, 11));
            btnActivate.setBounds(85, 35, 160, 25);
            btnActivate.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                }
            });
            add(btnActivate);

            JButton btnDeactivate = new JButton("Deactivate Assignment");
            //btnDeactivate.setFont(new Font("Tahoma", Font.PLAIN, 11));
            btnDeactivate.setBounds(255, 35, 165, 25);
            btnDeactivate.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                }
            });
            add(btnDeactivate);

            JList listFiles = new JList();
            listFiles.setBounds(20, 100, 440, 280);
            add(listFiles);

            JLabel lblFiles = new JLabel("Assignment Files:");
            //lblFiles.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblFiles.setBounds(10, 80, 120, 20);
            add(lblFiles);

            JButton btnBack = new JButton("< Back");
            //btnBack.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnBack.setBounds(15, 525, 89, 23);
            btnBack.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    professor.cardLayout.show(professor.pageHolder, "course");
                }
            });
            add(btnBack);

            JButton btnDropbox = new JButton("Dropbox >");
            //btnDropbox.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnDropbox.setBounds(375, 520, 99, 25);
            btnDropbox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    professor.cardLayout.show(professor.pageHolder, "submission");
                }
            });
            add(btnDropbox);

            JButton btnUploadFiles = new JButton("Upload Files");
            //btnUploadFiles.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnUploadFiles.setBounds(189, 392, 105, 23);
            btnUploadFiles.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                }
            });
            add(btnUploadFiles);

            JLabel lblDue = new JLabel("Due Date(MM/DD/YY):");
            lblDue.setFont(new Font("Tahoma", Font.BOLD, 15));
            lblDue.setBounds(69, 443, 185, 14);
            add(lblDue);

            JTextField textDue = new JTextField();
            textDue.setFont(new Font("Tahoma", Font.PLAIN, 15));
            textDue.setBounds(274, 440, 122, 20);
            add(textDue);
            textDue.setColumns(10);
        }
        else{
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "ASSIGNMENT"));
            setLayout(null);


            JLabel lblAssignment = new JLabel("AssignmentNameHere");
            lblAssignment.setHorizontalAlignment(SwingConstants.CENTER);
            lblAssignment.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblAssignment.setBounds(173, 22, 166, 14);
            add(lblAssignment);

            //test
            String[] array = new String[30];
            for(int i = 0; i<30; i++){
                array[i] = "hey";
            }


            JList listFiles = new JList(array);
            JScrollPane scrollFiles = new JScrollPane(listFiles);
            scrollFiles.setBounds(20, 100, 440, 280);
            add(scrollFiles);

            JLabel lblFiles = new JLabel("Assignment Files:");
            //lblFiles.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblFiles.setBounds(10, 80, 120, 20);
            add(lblFiles);

            JButton btnBack = new JButton("< Back");
            //btnBack.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnBack.setBounds(15, 525, 89, 23);
            btnBack.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    student.cardLayout.show(professor.pageHolder, "course");
                }
            });
            add(btnBack);

            JButton btnDropbox = new JButton("Dropbox >");
            //btnDropbox.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnDropbox.setBounds(375, 520, 99, 25);
            btnDropbox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                    student.cardLayout.show(professor.pageHolder, "submission");
                }
            });
            add(btnDropbox);

            JButton btnUploadFiles = new JButton("Download Files");
            //btnUploadFiles.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnUploadFiles.setBounds(180, 392, 120, 23);
            btnUploadFiles.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                }
            });
            add(btnUploadFiles);

            JLabel lblDue = new JLabel("Due Date(MM/DD/YY):");
            lblDue.setFont(new Font("Tahoma", Font.BOLD, 15));
            lblDue.setBounds(69, 443, 185, 14);
            add(lblDue);

            JTextField textDue = new JTextField(); // getDueDate from server
            textDue.setFont(new Font("Tahoma", Font.PLAIN, 15));
            textDue.setBounds(274, 440, 122, 20);
            textDue.setEditable(false);
            add(textDue);
            textDue.setColumns(10);
        }
    }
        /*panelTop = new JPanel(new GridBagLayout());
        panelCentre = new JPanel(new BorderLayout());
        panelBottom = new JPanel(new BorderLayout());
        panelBottomStuff = new JPanel(new GridBagLayout());
        panelBottomBack = new JPanel(new GridBagLayout());

        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Assignment"));
        setLayout(new BorderLayout());

        constraints = new GridBagConstraints();
        constraints.insets = new Insets(10,10,10,10);
        constraints.anchor = GridBagConstraints.CENTER;

        button1 = new JButton("Activate Assignment");
        button1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.out.println("pressed active");
                boolean check = professor.getClient().setCourseVisibility(courseID,false);
                if(check){
                    JOptionPane.showMessageDialog(null, "Set course: "+courseID+" to active", "Success", JOptionPane.INFORMATION_MESSAGE);
                }
                else{
                    JOptionPane.showMessageDialog(null, "Could not change visiblity", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        button2 = new JButton("Deactivate Assignment");
        button2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.out.println("pressed deactive");
                boolean check = professor.getClient().setCourseVisibility(courseID,false);
                if(check){
                    JOptionPane.showMessageDialog(null, "Set course: "+courseID+" to inactive", "Success", JOptionPane.INFORMATION_MESSAGE);
                }
                else{
                    JOptionPane.showMessageDialog(null, "Could not change visiblity", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }

        });
        panelTop.add(button1,constraints);
        panelTop.add(button2,constraints);

        list1 = new JList();
//        list1.addMouseListener(new MouseAdapter() {
//            public void mouseClicked(MouseEvent mouseEvent) {
//                    professor.pageHolder.add(professor.coursePage,"course");
//                    JList theList = (JList) mouseEvent.getSource();
//                    if (mouseEvent.getClickCount() == 2) {
//                            int index = theList.locationToIndex(mouseEvent.getPoint());
//                            if (index >= 0) {
//                                    Object o = theList.getModel().getElementAt(index);
//                                    System.out.println("Double-clicked on: " + o.toString());
//                                    professor.cardLayout.show(professor.pageHolder, "course");
//                            }
//                    }
//            }
//        });
        scrollPane1 = new JScrollPane(list1);
        label2 = new JLabel("Assignment Files:");
        panelCentre.add("North",label2);
        panelCentre.add("Center",scrollPane1);


        button3 = new JButton("Upload Files");
        button3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //TODO: FIX ME
//                File selectedFile = new File(" ");//pathname coming from a textfield use this, need this to dl to server
//                Assignment a = new Assignment(); // fill all the parameters from the textfields, need this for db
                JFileChooser fileBrowser = new JFileChooser();
                File selectedFile = null;
                if(fileBrowser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
                    selectedFile = fileBrowser.getSelectedFile();
                System.out.println("got file");
                professor.getClient().uploadAssignment(selectedFile); // add the assignment as the second param
            }

        });
        button4 = new JButton("Dropbox >>>");
        button4.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            professor.pageHolder.add(professor.submissionPage,"submission");
            System.out.println("pressed back");
            professor.cardLayout.show(professor.pageHolder,"submission");
            }

        });
        panelBottomStuff.add(button3,constraints);
        panelBottomStuff.add(button4,constraints);

        button5 = new JButton("< Back");
        button5.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                    professor.pageHolder.add(professor.homePage,"home");
                    System.out.println("pressed back");
                    professor.cardLayout.show(professor.pageHolder,"home");
            }

        });
        constraints.anchor = GridBagConstraints.WEST;
        panelBottomBack.add(button5, constraints);

        panelBottom.add("North",panelBottomStuff);
        panelBottom.add("West",panelBottomBack);


        add("North",panelTop);
        add("Center",panelCentre);
        add("South",panelBottom);
    }*/
        public void setProfessor(ProfessorGUI p){
            this.professor = p;
        }
        public void setStudent(StudentGUI s){
            this.student = s;
        }

        public static void main(String[] args){
	        AssignmentPage p = new AssignmentPage();
        }
}
