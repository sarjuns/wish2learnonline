package frontend.pages;

import java.awt.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

import frontend.ProfessorGUI;
import frontend.StudentGUI;
import shared.*;

public class HomePage extends Page{

	
	public HomePage() {
        if(isProfessor) {
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "HOME"));
            setLayout(null);

            JLabel lblWelcome = new JLabel("Welcome Professor!");
            //lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblWelcome.setBounds(180, 25, 151, 14);
            add(lblWelcome);


            //test
            String[] array = new String[30];
            for(int i = 0; i<30; i++){
                array[i] = "hey";
            }

            JList listCourse = new JList(array);
            JScrollPane scrollCourse = new JScrollPane(listCourse);
            scrollCourse.setBounds(20, 89, 450, 365);
            add(scrollCourse);
            listCourse.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent mouseEvent) {
                    professor.pageHolder.add(professor.coursePage,"course");
                    JList theList = (JList) mouseEvent.getSource();
                    if (mouseEvent.getClickCount() == 2) {
                        int index = theList.locationToIndex(mouseEvent.getPoint());
                        if (index >= 0) {
                            Object o = theList.getModel().getElementAt(index);
                            System.out.println("Double-clicked on2: " + o.toString());
                            professor.cardLayout.show(professor.pageHolder, "course");
                        }
                    }

                }
            });

            JLabel lblCourse = new JLabel("Here are your courses:");
            //lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblCourse.setBounds(10, 64, 130, 14);
            add(lblCourse);

            JButton btnAddCourse = new JButton("Add a Course");
            // btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnAddCourse.setBounds(135, 465, 110, 23);
            btnAddCourse.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    System.out.println("pressed add course");
                    NewCoursePage ncp = new NewCoursePage();
                    boolean check = professor.getClient().insertCourse(new Course(professor.getUsername(),ncp.getName(),ncp.getId(),"false"));
                    if(check){
                        JOptionPane.showMessageDialog(null, "Successfully added course", "Success", JOptionPane.INFORMATION_MESSAGE);
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Could not insert new course", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            });
            add(btnAddCourse);

            JButton btnBrowseCourse = new JButton("Browse Courses");
            //btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnBrowseCourse.setBounds(250, 465, 130, 23);
            btnBrowseCourse.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    System.out.println("pressed browse course");
                }
            });
            add(btnBrowseCourse);

            JButton btnLogOut = new JButton("Log Out");
            // btnLogOut.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnLogOut.setBounds(15, 525, 89, 23);
            btnLogOut.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                }
            });
            add(btnLogOut);
        }
        else{
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "HOME"));
            setLayout(null);

            JLabel lblWelcome = new JLabel("Welcome Student!");
            //lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblWelcome.setBounds(180, 25, 151, 14);
            add(lblWelcome);

            String[] array = new String[30];
            for(int i = 0; i<30; i++){
                array[i] = "hey";
            }

            JList listCourse = new JList(array);
            JScrollPane scrollCourse = new JScrollPane(listCourse);
            scrollCourse.setBounds(20, 89, 450, 365);
            add(scrollCourse);
            listCourse.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent mouseEvent) {
                    student.pageHolder.add(professor.coursePage,"course");
                    JList theList = (JList) mouseEvent.getSource();
                    if (mouseEvent.getClickCount() == 2) {
                        int index = theList.locationToIndex(mouseEvent.getPoint());
                        if (index >= 0) {
                            Object o = theList.getModel().getElementAt(index);
                            System.out.println("Double-clicked on2: " + o.toString());
                            student.cardLayout.show(professor.pageHolder, "course");
                        }
                    }

                }
            });

            JLabel lblCourse = new JLabel("Here are your courses:");
            //lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
            lblCourse.setBounds(10, 64, 130, 14);
            add(lblCourse);

            JButton btnLogOut = new JButton("Log Out");
            btnLogOut.setFont(new Font("Tahoma", Font.BOLD, 11));
            btnLogOut.setBounds(15, 525, 89, 23);
            btnLogOut.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                }
            });
            add(btnLogOut);
        }
	    /*CardLayout cl = new CardLayout();
	    JPanel a = new JPanel(cl);

		panelTop = new JPanel(new GridBagLayout());
		panelCentre = new JPanel(new BorderLayout());
		panelBottom = new JPanel(new GridBagLayout());
		
		setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "HOME"));
		setLayout(new BorderLayout());
		constraints = new GridBagConstraints();
		constraints.insets = new Insets(10,10,10,10);
		constraints.anchor = GridBagConstraints.CENTER;
		
		label1 = new JLabel("Welcome Professor !"); // take professor's name
		panelTop.add(label1,constraints);

        list1 = new JList(courseArray);
		button1 = new JButton("Add a Course");
        button1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.out.println("pressed add course");
                NewCoursePage ncp = new NewCoursePage();
                boolean check = professor.getClient().insertCourse(new Course(professor.getUsername(),ncp.getName(),ncp.getId(),"false"));
                if(check){
                    JOptionPane.showMessageDialog(null, "Successfully added course", "Success", JOptionPane.INFORMATION_MESSAGE);
                }
                else{
                    JOptionPane.showMessageDialog(null, "Could not insert new course", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        panelBottom.add(button1,constraints);
		constraints.anchor = GridBagConstraints.EAST;
		button2 = new JButton("Browse Courses");
        button2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                System.out.println("pressed browse course");
                courses = professor.getClient().getCourses(professor.getUsername());

                courseArray = new String[courses.size()];
                for(int i = 0; i < courseArray.length; i++){
                    courseArray[i] = courses.get(i).getName();
                }
                list1 = new JList(courseArray);
                list1.addMouseListener(new MouseAdapter() {
                    public void mouseClicked(MouseEvent mouseEvent) {
                        professor.pageHolder.add(professor.coursePage,"course");
                        JList theList = (JList) mouseEvent.getSource();
                        if (mouseEvent.getClickCount() == 2) {
                            int index = theList.locationToIndex(mouseEvent.getPoint());
                            if (index >= 0) {
                                System.out.println(courses.get(index).getName()+" "+courses.get(index).getCourse_id());
                                Object o = theList.getModel().getElementAt(index);
                                System.out.println("Double-clicked on: " + o.toString());
                                professor.coursePage.setCourseID(Integer.parseInt(courses.get(index).getCourse_id()));
                                professor.cardLayout.show(professor.pageHolder, "course");
                            }
                        }
                    }
                });
                scrollPane2 = new JScrollPane(list1);
                label2 = new JLabel("Here are your courses:");
                b.add("North",label2);
                b.add("Center", scrollPane2);
                cl.show(a,"test");


            }
        });
        list1.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                professor.pageHolder.add(professor.coursePage,"course");
                JList theList = (JList) mouseEvent.getSource();
                if (mouseEvent.getClickCount() == 2) {
                    int index = theList.locationToIndex(mouseEvent.getPoint());
                    if (index >= 0) {
                        Object o = theList.getModel().getElementAt(index);
                        System.out.println("Double-clicked on2: " + o.toString());
                        professor.cardLayout.show(professor.pageHolder, "course");
                    }
                }

            }
        });
        scrollPane1 = new JScrollPane(list1);
        label2 = new JLabel("Here are your courses:");
        panelCentre.add("North",label2);
        panelCentre.add("Center",scrollPane1);
        a.add(panelCentre,"centerPanel");
        a.add(b, "test");
        cl.show(a,"centerPanel");
        panelBottom.add(button2,constraints);


		add("North",panelTop);
		add("Center",a);
		add("South",panelBottom);

*/
	}

    public void setProfessor(ProfessorGUI p){
        this.professor = p;
    }
    public void setStudent(StudentGUI s){
        this.student = s;
    }
}
