package frontend.pages;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import frontend.ProfessorGUI;
import frontend.StudentGUI;
import shared.*;

public class GradePage extends Page {
	Course course;

	public GradePage() {
        if(isProfessor == true) {
                setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "GRADE"));
                setLayout(null);

                JLabel lblComments = new JLabel("Comments:");
                lblComments.setFont(new Font("Tahoma", Font.BOLD, 11));
                lblComments.setBounds(10, 74, 76, 14);
                add(lblComments);

                JLabel lblInsertSubmission = new JLabel("Insert Student's name here");
                lblInsertSubmission.setHorizontalAlignment(SwingConstants.CENTER);
                lblInsertSubmission.setFont(new Font("Tahoma", Font.BOLD, 11));
                lblInsertSubmission.setBounds(57, 34, 384, 14);
                add(lblInsertSubmission);

                JTextArea textArea = new JTextArea();
                textArea.setRows(10);
                textArea.setBounds(20, 99, 450, 306);
                add(textArea);

                JLabel lblGrade = new JLabel("Grade (in %):");
                lblGrade.setFont(new Font("Tahoma", Font.BOLD, 14));
                lblGrade.setBounds(140, 416, 110, 28);
                add(lblGrade);

                textField = new JTextField();
                textField.setFont(new Font("Tahoma", Font.PLAIN, 15));
                textField.setBounds(280, 420, 55, 20);
                add(textField);
                textField.setColumns(10);

                JButton btnSubmit = new JButton("Submit");
                btnSubmit.setFont(new Font("Tahoma", Font.BOLD, 11));
                btnSubmit.setBounds(209, 466, 89, 23);
                add(btnSubmit);

                JButton btnBack = new JButton("< Back");
                btnBack.setFont(new Font("Tahoma", Font.BOLD, 11));
                btnBack.setBounds(15, 525, 89, 23);
                btnBack.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent arg0) {
                        professor.cardLayout.show(professor.pageHolder, "submission");
                    }
                });
                add(btnBack);
        }
        else{
                setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "GRADE"));
                setLayout(null);

                JLabel lblComments = new JLabel("Comments:");
                lblComments.setFont(new Font("Tahoma", Font.BOLD, 11));
                lblComments.setBounds(10, 74, 76, 14);
                add(lblComments);

                JLabel lblInsertSubmission = new JLabel("Insert Student's name here");
                lblInsertSubmission.setHorizontalAlignment(SwingConstants.CENTER);
                lblInsertSubmission.setFont(new Font("Tahoma", Font.BOLD, 11));
                lblInsertSubmission.setBounds(57, 34, 384, 14);
                add(lblInsertSubmission);

                JTextArea textArea = new JTextArea();
                textArea.setRows(10);
                textArea.setBounds(20, 99, 450, 306);
                add(textArea);

                JLabel lblGrade = new JLabel("Grade (in %):");
                lblGrade.setFont(new Font("Tahoma", Font.BOLD, 14));
                lblGrade.setBounds(140, 416, 110, 28);
                add(lblGrade);

                textField = new JTextField();
                textField.setFont(new Font("Tahoma", Font.PLAIN, 15));
                textField.setBounds(280, 420, 55, 20);
                add(textField);
                textField.setColumns(10);

                JButton  btnBack = new JButton("< Back");
                btnBack.setFont(new Font("Tahoma", Font.BOLD, 11));
                btnBack.setBounds(15, 525, 89, 23);
                btnBack.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent arg0) {
                        student.cardLayout.show(professor.pageHolder, "submission");
                    }
                });
                add(btnBack);
        }
    }
    public void setProfessor(ProfessorGUI p){
        this.professor = p;
    }
    public void setStudent(StudentGUI s){
        this.student = s;
    }
}
