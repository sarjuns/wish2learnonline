package frontend.pages;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

import frontend.*;
import shared.Course;

public class Page extends JPanel{
	
	protected boolean isProfessor = true;
	protected StudentGUI student;
	protected ProfessorGUI professor;
	
	JPanel panelTop,panelTop1,panelTop2,panelCentre,panelBottom,panelBottomStuff,panelBottomBack;
	JPanel panel;


	GridBagConstraints constraints;

	protected JTextArea text1;
	protected JLabel label1, label2,label3;
	
	protected JScrollPane scrollPane1, scrollPane2;
    protected String[] courseArray = {" "};
	protected String[] studentsArray = {" "};
    protected ArrayList<Course> courses = null;
	protected ArrayList<Course> students = null;
    protected int courseID;
    protected int assignID;
    protected String[] studentArray = {" "};
	protected String[] assignmentArray  = {" "};
	protected JList list1, list2;
	
	protected JButton button1,button2,button3,button4,button5,button6;
	protected JTextField textField,textField1, textField2, textField3, textField4;
	protected JRadioButton radioButt1,radioButt2;
	protected ButtonGroup radioButtGroup;
	public Page() {

	}


	public void setCourseArray(String[] arr){
	    this.courseArray = arr;
    }
    public void setCourseID(int i){
		this.courseID = i;
	}
}
