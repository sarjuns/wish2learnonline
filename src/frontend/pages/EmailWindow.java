package frontend.pages;

import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;

import frontend.ProfessorGUI;
import frontend.components.EmailHelper;

import java.awt.Color;
import javax.swing.JPasswordField;
import javax.swing.JButton;

/**
 * @author ArjunS
 *
 */
public class EmailWindow extends Page {
	private JTextField emailUserName;
	private JPasswordField emailPassWord;
	private JTextField toRecipient;
	private JTextField subject;
	private JTextArea emailContent;
	private JButton sendEmailButton;
	private JButton cancelButton;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2154298670614672357L;

	/**
	 * 
	 */
	public EmailWindow() {
		setLayout(null);
		setSize(500, 570);

		JLabel title = new JLabel("Send email");
		title.setFont(new Font("Tahoma", Font.PLAIN, 30));
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setBounds(165, 15, 175, 40);
		add(title);

		JLabel userNameLabel = new JLabel("Gmail Id"); // ("Gmail/Yahoo Email");
		userNameLabel.setBounds(40, 75, 103, 20);
		add(userNameLabel);

		emailUserName = new JTextField();
		emailUserName.setBounds(150, 75, 305, 20);
		add(emailUserName);
		emailUserName.setColumns(40);

		JLabel passwordLabelName = new JLabel("Email Password");
		passwordLabelName.setBounds(40, 100, 103, 20);
		add(passwordLabelName);

		emailPassWord = new JPasswordField();
		emailPassWord.setBounds(150, 100, 305, 20);
		emailPassWord.setEchoChar('*');
		add(emailPassWord);
		emailPassWord.setColumns(40);

		JLabel lblNewLabel = new JLabel("Logging in for the 1st time ?! , Visit the url below and give access");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setBounds(40, 500, 415, 16);
		add(lblNewLabel);

		JTextField link = new JTextField();
		link.setBackground(Color.WHITE);
		link.setEditable(false);
		link.setText("https://www.google.com/settings/security/lesssecureapps");
		link.setBounds(40, 525, 415, 25);
		add(link);

		emailContent = new JTextArea();
		emailContent.setBounds(40, 185, 415, 255);
		add(emailContent);

		cancelButton = new JButton("Cancel");
		cancelButton.setBounds(345, 450, 100, 25);
		add(cancelButton);

		sendEmailButton = new JButton("Send Email");
		sendEmailButton.setBounds(215, 450, 100, 25);
		add(sendEmailButton);

		toRecipient = new JTextField();
		toRecipient.setColumns(40);
		toRecipient.setBounds(150, 136, 305, 20);
		add(toRecipient);

		subject = new JTextField();
		subject.setColumns(40);
		subject.setBounds(150, 158, 305, 20);
		add(subject);

		JLabel toLabel = new JLabel("  TO");
		toLabel.setBounds(38, 137, 105, 19);
		add(toLabel);

		JLabel subslabel = new JLabel(" Subject");
		subslabel.setBounds(40, 160, 103, 16);
		add(subslabel);

		// --------------------- Action Listeners
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				professor.cardLayout.show(professor.pageHolder, "course");

			}
		});

		// -------------
		sendEmailButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				EmailHelper newEmail = new EmailHelper(toRecipient.getText(), emailUserName.getText(),
						emailContent.getText(), subject.getText(), String.valueOf(emailPassWord.getPassword()));
			}
		});
	}

	public void setProfessor(ProfessorGUI p) {
		this.professor = p;
	}
}
