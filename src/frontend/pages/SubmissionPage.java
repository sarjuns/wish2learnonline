package frontend.pages;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import frontend.ProfessorGUI;
import frontend.StudentGUI;
import shared.*;

public class SubmissionPage extends Page {
        Course course;
        Submission submission;
        Assignment assignment;

        public SubmissionPage() {
                if(isProfessor == true) {
                        setLayout(null);

                        JLabel lblAssignmentName = new JLabel("Insert AssignmentName Here");
                        //lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
                        lblAssignmentName.setBounds(153, 29, 191, 14);
                        add(lblAssignmentName);

                        JList list = new JList();
                        list.setBounds(20, 91, 452, 376);
                        add(list);

                        JLabel lblStudentSubmissions = new JLabel("Student Submissions:");
                        //lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
                        lblStudentSubmissions.setBounds(10, 66, 134, 14);
                        add(lblStudentSubmissions);

                        JButton btnViewSubmission = new JButton("View Submission");
                        //btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
                        btnViewSubmission.setBounds(113, 484, 134, 23);
                        add(btnViewSubmission);

                        JButton btnGradeSubmission = new JButton("Grade Submission");
                        //btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 11));
                        btnGradeSubmission.setBounds(257, 484, 140, 23);
                        btnGradeSubmission.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent arg0) {
                                        professor.cardLayout.show(professor.pageHolder, "grade");
                                }
                        });
                        add(btnGradeSubmission);

                        JButton btnBack = new JButton("< Back");
                        //btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 11));
                        btnBack.setBounds(15, 525, 89, 23);
                        btnBack.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent arg0) {
                                        professor.cardLayout.show(professor.pageHolder, "assignment");
                                }
                        });
                        add(btnBack);
                }
                else{
                        setLayout(null);

                        JLabel lblAssignmentName = new JLabel("Insert AssignmentName Here");
                        lblAssignmentName.setFont(new Font("Tahoma", Font.BOLD, 11));
                        lblAssignmentName.setBounds(153, 29, 191, 14);
                        add(lblAssignmentName);

                        JList list = new JList();
                        list.setBounds(20, 91, 452, 376);
                        add(list);

                        JLabel lblFileSubmissions = new JLabel("File Submissions:");
                        lblFileSubmissions.setFont(new Font("Tahoma", Font.BOLD, 11));
                        lblFileSubmissions.setBounds(10, 66, 134, 14);
                        add(lblFileSubmissions);

                        JButton btnUploadFiles = new JButton("Upload Files");
                        btnUploadFiles.setFont(new Font("Tahoma", Font.BOLD, 11));
                        btnUploadFiles.setBounds(113, 484, 134, 23);
                        add(btnUploadFiles);

                        JButton btnSubmit = new JButton("Submit");
                        btnSubmit.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent arg0) {
                                        //send to server?
                                }
                        });
                        btnSubmit.setFont(new Font("Tahoma", Font.BOLD, 11));
                        btnSubmit.setBounds(257, 484, 144, 23);
                        add(btnSubmit);

                        JButton btnBack = new JButton("< Back");
                        btnBack.setFont(new Font("Tahoma", Font.BOLD, 11));
                        btnBack.setBounds(15, 525, 89, 23);
                        btnBack.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent arg0) {
                                        student.cardLayout.show(professor.pageHolder, "assignment");
                                }
                        });
                        add(btnBack);

                    JButton btnViewMark = new JButton("View Mark >");
                        btnViewMark.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg0) {
                            student.cardLayout.show(professor.pageHolder, "grade");
                        }
                    });
                        btnViewMark.setFont(new Font("Tahoma", Font.BOLD, 11));
                        btnViewMark.setBounds(372, 520, 107, 23);
                        add(btnViewMark);
                }

        }
        public void setProfessor(ProfessorGUI p){
                this.professor = p;
        }
        public void setStudent(StudentGUI s){
                this.student = s;
        }

}



