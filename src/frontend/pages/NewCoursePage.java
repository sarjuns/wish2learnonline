package frontend.pages;

import javax.swing.*;

public class NewCoursePage {
    private String name;
    private String id;
    
    public NewCoursePage(){
        JTextField nameField = new JTextField(7);
        JTextField idField = new JTextField(7);

        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("Course Name:"));
        myPanel.add(nameField);
        myPanel.add(Box.createHorizontalStrut(15)); // a spacer
        myPanel.add(new JLabel("Course ID:"));
        myPanel.add(idField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Enter a new course", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            this.name = nameField.getText();
            this.id = idField.getText();
            System.out.println("class name: " + name);
            System.out.println("class id: " + id);
        }
    }

    public String getName() {
        return this.name;
    }

    public String getId() {
        return this.id;
    }

    public static void main (String[] args){
        NewCoursePage a = new NewCoursePage();
    }
}
