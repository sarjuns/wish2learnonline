package frontend;

interface CommandConstants {

    final static String[] COMMANDS = {"CHECK","GET_COURSES","UPLOADING_ASSIGN","ADD_COURSE","GET_STUDENT","CHANGE_VISIBLE"};

}
