/**
 * 
 */
package junkTestSamples;

import org.jasypt.util.password.StrongPasswordEncryptor;

/**
 * @author ArjunS
 *
 */
public class PasswordTest {

	/**
	 * 
	 */
	public PasswordTest() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String userPassword = "Arjun";
		
		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		String encryptedPassword = passwordEncryptor.encryptPassword(userPassword);

		if (passwordEncryptor.checkPassword("Arjun", encryptedPassword)) {
		  System.out.println("Success");
		} else {
			System.out.println("fail");
		}

	}

}
