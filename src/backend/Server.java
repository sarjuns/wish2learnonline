package backend;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// Assume all necessary file are imported
public class Server {
    PrintWriter out;
    Socket aSocket;
    ServerSocket serverSocket;
    BufferedReader in;
    ExecutorService pool;

    DBHelper database;
    FileHelper fileHelper;

    public Server() { // throws IOException {
        try {
            serverSocket = new ServerSocket(9090);
            pool = Executors.newCachedThreadPool();
            database = new DBHelper();
            database.fillUp();
        } catch (IOException e) {
            System.out.println("Error creating a new server socket");
        }
        System.out.println("Server is running");
    }

    public void communicate() throws IOException{
        try {
            String line = "";
            while (true) {
                aSocket = serverSocket.accept();
                System.out.println("after accept");
                in = new BufferedReader(new InputStreamReader(aSocket.getInputStream()));
                out = new PrintWriter((aSocket.getOutputStream()), true);

                Worker thread = new Worker(aSocket, database, fileHelper);

                pool.execute(thread);

            }
        } catch(IOException e){
            System.out.println(e.getMessage());
            // Stop accepting new clients and finish any active ones, then shutdown the threadpool.
            pool.shutdown();
            try {
                in.close();
                out.close();
                aSocket.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Server myserver = new Server();
        myserver.communicate();
    }
}
