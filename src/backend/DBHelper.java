package backend;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.jasypt.util.password.StrongPasswordEncryptor;

import shared.Assignment;
import shared.Course;
import shared.Grade;
import shared.StudentEnrollment;
import shared.Submission;
import shared.User;

/**
 * 
 */

/**
 * @author ArjunS
 *
 */
public class DBHelper implements Constants {

	private PreparedStatement us; // updateStatement
	private Connection conn;
	StrongPasswordEncryptor passwordEncryptor; // used for password encryption

	/**
	 * Default Constructor :Creates database and adds all the tables
	 */
	public DBHelper() {
		boolean success = false;
		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			try {
				conn = DriverManager.getConnection(URL + "/" + DATABASENAME, USER, PASSWORD);
			} catch (Exception e0) {
				us = conn.prepareStatement("CREATE DATABASE " + DATABASENAME);
				us.executeUpdate();
				success = true;
			} finally {
				System.out.println("Data Base Created");
			}
			conn = DriverManager.getConnection(URL + "/" + DATABASENAME, USER, PASSWORD);

		} catch (Exception e1) {
			success = false;
			System.out.println(e1);
			e1.printStackTrace();
		} finally {
			if (success)
				System.out.println("Connected to client database successfully");
		}
		createTables();
	}

	// ----------------------------------------------

	/**
	 * Creates all tables
	 */
	public void createTables() {

		createUserTable();
		createAssignmentTable();
		createGradeTable();
		createCourseTable();
		createSubmissinTable();
		createStudentEnrollmentTable();
	}

	/**
	 * 
	 * create a databse table of users if not previously existing, if previously
	 * existing ignored
	 * 
	 * @return
	 */
	public boolean createUserTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[0];

			// field1
			String field1Name = COL[0][0];
			String field1Type = "int ";
			String notNull = "NOT NULL ";

			String auto = AUTO;

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[0][1];
			String field2Type = "varchar(200) "; // password varcar(20) but encryopted varchar of more

			String field2 = field2Name + field2Type;

			// field3
			String field3Name = COL[0][2];
			String field3Type = "varchar(50) ";

			String field3 = field3Name + field3Type;

			// field4
			String field4Name = COL[0][3];
			String field4Type = "varchar(30) ";

			String field4 = field4Name + field4Type;

			// field5
			String field5Name = COL[0][4];
			String field5Type = "varchar(30) ";

			String field5 = field5Name + field5Type;

			// field6
			String field6Name = COL[0][5];
			String field6Type = "char(1) ";

			String field6 = field6Name + field6Type;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", " + field4
					+ ", " + field5 + ", " + field6 + ", " + primeVariable + " )";

			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();

			PreparedStatement alterTable = conn
					.prepareStatement("ALTER TABLE " + TABLENAMES[0] + " AUTO_INCREMENT =  " + AUTO_VAL);
			alterTable.executeUpdate();

			success = true;

		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}
		return success;

	}

	/**
	 * create a databse table of Assignment if not previously existing, if
	 * previously existing ignored
	 * 
	 * @return
	 */
	public boolean createAssignmentTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[1];

			// field1
			String field1Name = COL[1][0];
			String field1Type = "int(8) ";
			String notNull = "NOT NULL ";
			String auto = AUTO;

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[1][1];
			String field2Type = "int(8) ";

			String field2 = field2Name + field2Type + notNull;

			// field3
			String field3Name = COL[1][2];
			String field3Type = "varchar(50) ";

			String field3 = field3Name + field3Type;

			// field4
			String field4Name = COL[1][3];
			;
			String field4Type = "varchar(100) ";

			String field4 = field4Name + field4Type;

			// field5
			String field5Name = COL[1][4];
			;
			String field5Type = "int(1) ";

			String field5 = field5Name + field5Type + notNull;

			// field6
			String field6Name = COL[1][5];
			String field6Type = "char(16) ";

			String field6 = field6Name + field6Type;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", " + field4
					+ ", " + field5 + ", " + field6 + ", " + primeVariable + " )";

			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();

			PreparedStatement alterTable = conn
					.prepareStatement("ALTER TABLE " + TABLENAMES[1] + " AUTO_INCREMENT =  " + AUTO_VAL);
			alterTable.executeUpdate();

			success = true;
		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}
		return success;

	}

	/**
	 * 
	 * create a databse table of Grades if not previously existing, if previously
	 * existing ignored
	 * 
	 * @return
	 */
	public boolean createGradeTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[2];

			// field1
			String field1Name = COL[2][0];
			String field1Type = "int(8) ";
			String notNull = "NOT NULL ";
			String auto = AUTO;

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[2][1];
			String field2Type = "int(8) ";

			String field2 = field2Name + field2Type + notNull;

			// field3
			String field3Name = COL[2][2];
			String field3Type = "int(8) ";

			String field3 = field3Name + field3Type + notNull;

			// field4
			String field4Name = COL[2][3];
			String field4Type = "int(8) ";

			String field4 = field4Name + field4Type + notNull;

			// field5
			String field5Name = COL[2][4];
			String field5Type = "int(3) ";

			String field5 = field5Name + field5Type;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", " + field4
					+ ", " + field5 + ", " + primeVariable + " )";

			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();
			success = true;

			PreparedStatement alterTable = conn
					.prepareStatement("ALTER TABLE " + TABLENAMES[2] + " AUTO_INCREMENT =  " + AUTO_VAL);
			alterTable.executeUpdate();

		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}

		return success;
	}

	/**
	 * 
	 * create a databse table of Courses if not previously existing, if previously
	 * existing ignored
	 * 
	 * @return
	 */
	public boolean createCourseTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[3];

			// field1
			String field1Name = COL[3][0];
			String field1Type = "int(8) ";
			String notNull = "NOT NULL ";
			String auto = AUTO;

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[3][1];
			String field2Type = "int(8) ";

			String field2 = field2Name + field2Type + notNull;

			// field3
			String field3Name = COL[3][2];
			String field3Type = "varchar(50) ";

			String field3 = field3Name + field3Type;

			// field4
			String field4Name = COL[3][3];
			String field4Type = "int(1) ";

			String field4 = field4Name + field4Type + notNull;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", " + field4
					+ ", " + primeVariable + " )";
			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();

			PreparedStatement alterTable = conn
					.prepareStatement("ALTER TABLE " + TABLENAMES[3] + " AUTO_INCREMENT =  " + AUTO_VAL);
			alterTable.executeUpdate();

			success = true;

		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}

		return success;
	}

	/**
	 * 
	 * create a databse table of Submissins if not previously existing, if
	 * previously existing ignored
	 * 
	 * @return
	 */
	public boolean createSubmissinTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[4];

			// field1
			String field1Name = COL[4][0];
			String field1Type = "int(8) ";
			String notNull = "NOT NULL ";
			String auto = AUTO;

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[4][1];
			String field2Type = "int(8) ";

			String field2 = field2Name + field2Type + notNull;

			// field3
			String field3Name = COL[4][2];
			String field3Type = "int(8) ";

			String field3 = field3Name + field3Type + notNull;

			// field4
			String field4Name = COL[4][3];
			String field4Type = "varchar(100) ";

			String field4 = field4Name + field4Type;

			// field5
			String field5Name = COL[4][4];
			String field5Type = "varchar(50) ";

			String field5 = field5Name + field5Type;

			// field6
			String field6Name = COL[4][5];
			String field6Type = "int(3) ";

			String field6 = field6Name + field6Type;

			// field7
			String field7Name = COL[4][6];
			String field7Type = "varchar(140) ";

			String field7 = field7Name + field7Type;

			// field8
			String field8Name = COL[4][7];
			String field8Type = "char(16) ";

			String field8 = field8Name + field8Type;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", " + field4
					+ ", " + field5 + ", " + field6 + ", " + field7 + ", " + field8 + ", " + primeVariable + " )";

			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();

			PreparedStatement alterTable = conn
					.prepareStatement("ALTER TABLE " + TABLENAMES[4] + " AUTO_INCREMENT =  " + AUTO_VAL);
			alterTable.executeUpdate();

			success = true;

		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}

		return success;
	}

	/**
	 * 
	 * create a databse table of StudentEnrollments if not previously existing, if
	 * previously existing ignored
	 * 
	 * @return
	 */
	public boolean createStudentEnrollmentTable() {
		boolean success = false;

		try {
			// -------------------------------------------
			String condition = "CREATE TABLE IF NOT EXISTS ";
			String tableName = TABLENAMES[5];

			// field1
			String field1Name = COL[5][0];
			String field1Type = "int(8) ";
			String notNull = "NOT NULL ";
			String auto = AUTO;

			String field1 = field1Name + field1Type + notNull + auto;

			// field2
			String field2Name = COL[5][1];
			String field2Type = "int(8) ";

			String field2 = field2Name + field2Type + notNull;

			// field3
			String field3Name = COL[5][2];
			String field3Type = "int(8) ";
			;

			String field3 = field3Name + field3Type + notNull;

			// primaryKey
			String primaryField = field1Name;
			String primaryKey = "PRIMARY KEY";
			String primeVariable = primaryKey + "(" + primaryField + ")";

			String statement = condition + tableName + "( " + field1 + ", " + field2 + ", " + field3 + ", "
					+ primeVariable + " )";

			PreparedStatement createTable = conn.prepareStatement(statement);
			createTable.executeUpdate();

			PreparedStatement alterTable = conn
					.prepareStatement("ALTER TABLE " + TABLENAMES[5] + " AUTO_INCREMENT =  " + AUTO_VAL);
			alterTable.executeUpdate();
			success = true;

		} catch (Exception e1) {
			System.out.println(e1);
			e1.printStackTrace();
			success = false;
		}

		return success;
	}

	// ----------------------------------------------------

	/**
	 * adds a new User to database
	 * 
	 * @param us
	 *            : User to add
	 * @return
	 */
	public boolean addUser(User us) {
		String userPassword = us.getPassword();
		passwordEncryptor = new StrongPasswordEncryptor();
		String encryptedPassword = passwordEncryptor.encryptPassword(userPassword);

		String fieldData[] = { encryptedPassword, us.getEmail(), us.getFirstname(), us.getLastname(), us.getType() };
		String fieldNameString = " ( " + COL[0][1] + " , " + COL[0][2] + " , " + COL[0][3] + " , " + COL[0][4] + " , "
				+ COL[0][5] + " ) ";

		String tableName = TABLENAMES[0];
		boolean success = false;
		try {

			PreparedStatement ps = conn
					.prepareStatement("INSERT INTO " + tableName + fieldNameString + " VALUES ( ?, ?, ?, ?, ? ) ");
			ps.setString(1, fieldData[0]); // pasword
			ps.setString(2, fieldData[1]); // email
			ps.setString(3, fieldData[2]); // firstname
			ps.setString(4, fieldData[3]); // lastname
			ps.setString(5, fieldData[4]); // type
			ps.executeUpdate();
			success = true;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			success = false;
		}
		return success;

	}

	/**
	 * adds a new Assignment to database
	 * 
	 * @param as
	 *            : Assignment to add
	 * @return
	 */
	public boolean addAssignment(Assignment as) {
		String fieldData[] = { as.getCourse_id(), as.getTitle(), as.getPath(), as.getBit(), as.getDue_dateString() };
		String fieldNameString = " ( " + COL[1][1] + " , " + COL[1][2] + " , " + COL[1][3] + " , " + COL[1][4] + " , "
				+ COL[1][5] + " ) ";

		String tableName = TABLENAMES[1];
		boolean success = false;
		try {

			PreparedStatement ps = conn
					.prepareStatement("INSERT INTO " + tableName + fieldNameString + " VALUES ( ?, ?, ?, ?, ? ) ");
			ps.setString(1, fieldData[0]); // course_id
			ps.setString(2, fieldData[1]); // title
			ps.setString(3, fieldData[2]); // path
			ps.setString(4, fieldData[3]); // active
			ps.setString(5, fieldData[4]); // due_date
			ps.executeUpdate();
			success = true;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			success = false;
		}
		return success;

	}

	/**
	 * adds a new Grade to database
	 * 
	 * @param gs
	 *            : Grade to add
	 * @return
	 */
	public boolean addGrade(Grade gs) {
		String fieldData[] = { gs.getAssign_id(), gs.getStudent_id(), gs.getCourse_id(), gs.getAssignment_grade() };
		String fieldNameString = " ( " + COL[2][1] + " , " + COL[2][2] + " , " + COL[2][3] + " , " + COL[2][4] + " ) ";

		String tableName = TABLENAMES[2];
		boolean success = false;
		try {

			PreparedStatement ps = conn
					.prepareStatement("INSERT INTO " + tableName + fieldNameString + " VALUES ( ?, ?, ?, ? ) ");
			ps.setString(1, fieldData[0]); // assign_id
			ps.setString(2, fieldData[1]); // student_id
			ps.setString(3, fieldData[2]); // course_id
			ps.setString(4, fieldData[3]); // assignment_grade
			ps.executeUpdate();
			success = true;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			success = false;
		}
		return success;

	}

	/**
	 * adds a new Course to database
	 * 
	 * @param cs
	 *            : Course to add
	 * @return
	 */
	public boolean addCourse(Course cs) {
		String fieldData[] = { cs.getProf_int(), cs.getName(), cs.getCourse_id() };
		String fieldNameString = " ( " + COL[3][1] + " , " + COL[3][2] + " , " + COL[3][3] + " ) ";

		String tableName = TABLENAMES[3];
		boolean success = false;
		try {

			PreparedStatement ps = conn
					.prepareStatement("INSERT INTO " + tableName + fieldNameString + " VALUES ( ?, ?, ? ) ");
			ps.setString(1, fieldData[0]); // prof_id
			ps.setString(2, fieldData[1]); // name
			ps.setString(3, fieldData[2]); // active
			ps.executeUpdate();
			success = true;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			success = false;
		}
		return success;

	}

	/**
	 * adds a new Submission to database
	 * 
	 * @param us
	 *            : Submission to add
	 * @return
	 */
	public boolean addSubmission(Submission us) {
		String fieldData[] = { us.getAssign_id(), us.getStudent_id(), us.getPath(), us.getTitle(),
				us.getSubmission_grade(), us.getComments(), us.getTimestamp() };
		String fieldNameString = " ( " + COL[4][1] + " , " + COL[4][2] + " , " + COL[4][3] + " , " + COL[4][4] + " , "
				+ COL[4][5] + " , " + COL[4][6] + " , " + COL[4][7] + " ) ";

		String tableName = TABLENAMES[4];
		boolean success = false;
		try {

			PreparedStatement ps = conn.prepareStatement(
					"INSERT INTO " + tableName + fieldNameString + " VALUES ( ?, ?, ?, ?, ?, ?, ? ) ");
			ps.setString(1, fieldData[0]); // assign_id
			ps.setString(2, fieldData[1]); // student_id
			ps.setString(3, fieldData[2]); // path
			ps.setString(4, fieldData[3]); // title
			ps.setString(5, fieldData[4]); // submission_grade
			ps.setString(6, fieldData[5]); // comments
			ps.setString(7, fieldData[6]); // timestamp
			ps.executeUpdate();
			success = true;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			success = false;
		}
		return success;

	}

	/**
	 * adds a new StudentEnrollment to database
	 * 
	 * @param se
	 *            : StudentEnrollment to add
	 * @return
	 */
	public boolean addStudentEnrollment(StudentEnrollment se) {
		String fieldData[] = { se.getstudent_id(), se.getCourse_id() };
		String fieldNameString = " ( " + COL[5][1] + " , " + COL[5][2] + " ) ";

		String tableName = TABLENAMES[5];
		boolean success = false;
		try {

			PreparedStatement ps = conn
					.prepareStatement("INSERT INTO " + tableName + fieldNameString + " VALUES ( ?, ? ) ");
			ps.setString(1, fieldData[0]); // prof_id
			ps.setString(2, fieldData[1]); // course_id
			ps.executeUpdate();
			success = true;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			success = false;
		}
		return success;

	}

	// ------------------------------------------------------

	/**
	 * @return gets all users from data base
	 */
	@SuppressWarnings("unused")
	private ArrayList<User> getAllUsers() {
		String tableName = TABLENAMES[0];
		String searchStatement = "SELECT * FROM " + tableName;
		String fieldName[] = COL[0]; // 6

		try {
			ResultSet usersInfo = null;
			ArrayList<User> cl = new ArrayList<User>();
			us = conn.prepareStatement(searchStatement);
			usersInfo = us.executeQuery();
			while (usersInfo.next()) {
				User us = new User(usersInfo.getString(fieldName[1]), usersInfo.getString(fieldName[2]),
						usersInfo.getString(fieldName[3]), usersInfo.getString(fieldName[4]),
						usersInfo.getString(fieldName[5]));
				us.setId(usersInfo.getString(fieldName[0]));
				cl.add(us);
			}

			return cl;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @param id
	 *            : id of User to be searched
	 * @return : User corresponding to id
	 */
	public User searchUserbyID(String id) {
		String tableName = TABLENAMES[0];
		String searchStatement = "SELECT * FROM " + tableName + " WHERE id = '" + id + "'";
		String fieldName[] = COL[0]; // 6

		try {
			ResultSet usersInfo = null;
			us = conn.prepareStatement(searchStatement);
			usersInfo = us.executeQuery();
			usersInfo.next();
			User us = new User(usersInfo.getString("password"), usersInfo.getString("email"),
					usersInfo.getString("firstname"), usersInfo.getString("lastname"), usersInfo.getString("type"));
			us.setId(usersInfo.getString("id"));

			return us;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param email
	 *            : email of User to be searched
	 * @return : User corresponding to email
	 */
	public ArrayList<User> searchUserByEmail(String email) {
		String tableName = TABLENAMES[0];
		String searchStatement = "SELECT * FROM " + tableName + " WHERE " + COL[0][2] + " =  '" + email + "'";
		String fieldName[] = COL[0]; // 6

		try {
			ResultSet usersInfo = null;
			ArrayList<User> cl = new ArrayList<User>();
			us = conn.prepareStatement(searchStatement);
			usersInfo = us.executeQuery();
			while (usersInfo.next()) {
				User us = new User(usersInfo.getString(fieldName[1]), usersInfo.getString(fieldName[2]),
						usersInfo.getString(fieldName[3]), usersInfo.getString(fieldName[4]),
						usersInfo.getString(fieldName[5]));
				us.setId(usersInfo.getString(fieldName[0]));
				cl.add(us);
			}

			return cl;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @param name
	 *            : name of User to be searched
	 * @return : User corresponding to name
	 */
	public ArrayList<User> searchUser(String name) {
		String tableName = TABLENAMES[0];
		String searchStatement = "SELECT * FROM " + tableName + " WHERE " + COL[0][3] + " =  '" + name + "'" + " OR "
				+ COL[0][4] + " =  '" + name + "'";
		String fieldName[] = COL[0]; // 6

		try {
			ResultSet usersInfo = null;
			ArrayList<User> cl = new ArrayList<User>();
			us = conn.prepareStatement(searchStatement);
			usersInfo = us.executeQuery();
			while (usersInfo.next()) {
				User us = new User(usersInfo.getString(fieldName[1]), usersInfo.getString(fieldName[2]),
						usersInfo.getString(fieldName[3]), usersInfo.getString(fieldName[4]),
						usersInfo.getString(fieldName[5]));
				us.setId(usersInfo.getString(fieldName[0]));
				cl.add(us);
			}

			return cl;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}

	// ------------------------------------------------------

	/**
	 * @return gets all users from data base
	 */
	@SuppressWarnings("unused")
	private ArrayList<Assignment> getAllAssignments() {
		String tableName = TABLENAMES[1];
		String searchStatement = "SELECT * FROM " + tableName;
		String fieldName[] = COL[1]; // 6

		try {
			ResultSet assignmentInfo = null;
			ArrayList<Assignment> cl = new ArrayList<Assignment>();
			us = conn.prepareStatement(searchStatement);
			assignmentInfo = us.executeQuery();
			while (assignmentInfo.next()) {
				Assignment as = new Assignment(assignmentInfo.getString(fieldName[1]),
						assignmentInfo.getString(fieldName[2]), assignmentInfo.getString(fieldName[3]),
						assignmentInfo.getString(fieldName[4]), assignmentInfo.getString(fieldName[5]));
				as.setId(assignmentInfo.getString(fieldName[0]));
				cl.add(as);
			}

			return cl;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @param id
	 *            : id of Assignmnet to be searched
	 * @return : Assignmnet corresponding to id
	 */
	public ArrayList<Assignment> searchAssignment(int id) {
		String tableName = TABLENAMES[1];
		String searchStatement = "SELECT * FROM " + tableName + " WHERE id = '" + id + "'";
		String fieldName[] = COL[1]; // 6

		try {
			ResultSet assignmentInfo = null;
			ArrayList<Assignment> cl = new ArrayList<Assignment>();
			us = conn.prepareStatement(searchStatement);
			assignmentInfo = us.executeQuery();
			while (assignmentInfo.next()) {
				Assignment as = new Assignment(assignmentInfo.getString(fieldName[1]),
						assignmentInfo.getString(fieldName[2]), assignmentInfo.getString(fieldName[3]),
						assignmentInfo.getString(fieldName[4]), assignmentInfo.getString(fieldName[5]));
				as.setId(assignmentInfo.getString(fieldName[0]));
				cl.add(as);
			}

			return cl;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @param course_id
	 *            : course_id of Assignment to be searched
	 * @return : User corresponding to course_id
	 */
	public ArrayList<Assignment> searchAssignmentByCourse(int course_id) {
		String tableName = TABLENAMES[1];
		String searchStatement = "SELECT * FROM " + tableName + " WHERE " + COL[1][1] + " = '" + course_id + "'";
		String fieldName[] = COL[1]; // 6

		try {
			ResultSet assignmentInfo = null;
			ArrayList<Assignment> cl = new ArrayList<Assignment>();
			us = conn.prepareStatement(searchStatement);
			assignmentInfo = us.executeQuery();
			while (assignmentInfo.next()) {
				Assignment as = new Assignment(assignmentInfo.getString(fieldName[1]),
						assignmentInfo.getString(fieldName[2]), assignmentInfo.getString(fieldName[3]),
						assignmentInfo.getString(fieldName[4]), assignmentInfo.getString(fieldName[5]));
				as.setId(assignmentInfo.getString(fieldName[0]));
				cl.add(as);
			}

			return cl;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}

	// ------------------------------------------------------

	/**
	 * @return gets all Grade from data base
	 */
	@SuppressWarnings("unused")
	private ArrayList<Grade> getAllGrades() {
		String tableName = TABLENAMES[2];
		String searchStatement = "SELECT * FROM " + tableName;
		String fieldName[] = COL[2]; // 6
		try {
			ResultSet gradesInfo = null;
			ArrayList<Grade> cl = new ArrayList<Grade>();
			us = conn.prepareStatement(searchStatement);
			gradesInfo = us.executeQuery();
			while (gradesInfo.next()) {
				Grade gs = new Grade(gradesInfo.getString(fieldName[1]), gradesInfo.getString(fieldName[2]),
						gradesInfo.getString(fieldName[3]), gradesInfo.getString(fieldName[4]));
				gs.setId(gradesInfo.getString(fieldName[0]));
				cl.add(gs);
			}
			return cl;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}

	// ------------------------------------------------------

	/**
	 * @return gets all Courses from data base
	 */
	@SuppressWarnings("unused")
	private ArrayList<Course> getAllCourses() {
		String tableName = TABLENAMES[3];
		String searchStatement = "SELECT * FROM " + tableName;
		String fieldName[] = COL[3]; // 6
		try {
			ResultSet courseInfo = null;
			ArrayList<Course> cl = new ArrayList<Course>();
			us = conn.prepareStatement(searchStatement);
			courseInfo = us.executeQuery();
			while (courseInfo.next()) {
				Course cs = new Course(courseInfo.getString("prof_id"), courseInfo.getString("name"),
						courseInfo.getString("id"), courseInfo.getString("active"));
				// cs.setId(courseInfo.getString(fieldName[0]));
				cl.add(cs);
			}
			return cl;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return null;
	}

	public void changeVisibility(String courseID, String cond) {
		String tableName = TABLENAMES[3];
		System.out.println("in db visble " + courseID + " " + cond);
		// UPDATE wish2learn.coursetable SET active='1' WHERE id='3'
		String searchStatement = "UPDATE " + tableName + " SET active='" + cond + "' WHERE id='" + courseID + "'";
		try {
			us = conn.prepareStatement(searchStatement);
			us.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @return gets all Courses from data base
	 */
	@SuppressWarnings("unused")
	public ArrayList<Course> getCourses(String prof_id) {
		String tableName = TABLENAMES[3];
		String searchStatement = "SELECT * FROM " + tableName + " WHERE " + COL[3][1] + " = '" + prof_id + "'";
		String fieldName[] = COL[3]; // 6
		try {
			ResultSet courseInfo = null;
			ArrayList<Course> cl = new ArrayList<Course>();
			us = conn.prepareStatement(searchStatement);
			courseInfo = us.executeQuery();
			while (courseInfo.next()) {
				Course cs = new Course(courseInfo.getString("prof_id"), courseInfo.getString("name"),
						courseInfo.getString("id"), courseInfo.getString("active"));
				// cs.setId(courseInfo.getString("id"));
				cl.add(cs);
			}
			return cl;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return null;
	}

	// ------------------------------------------------------

	/**
	 * @return gets all Submission from data base
	 */
	@SuppressWarnings("unused")
	private ArrayList<Submission> getAllSubmission() {
		String tableName = TABLENAMES[4];
		String searchStatement = "SELECT * FROM " + tableName;
		String fieldName[] = COL[4]; // 6
		try {
			ResultSet submissionInfo = null;
			ArrayList<Submission> cl = new ArrayList<Submission>();
			us = conn.prepareStatement(searchStatement);
			submissionInfo = us.executeQuery();
			while (submissionInfo.next()) {
				Submission sub = new Submission(submissionInfo.getString(fieldName[1]),
						submissionInfo.getString(fieldName[2]), submissionInfo.getString(fieldName[3]),
						submissionInfo.getString(fieldName[4]), submissionInfo.getString(fieldName[5]),
						submissionInfo.getString(fieldName[6]), submissionInfo.getString(fieldName[7]));
				sub.setId(submissionInfo.getString(fieldName[0]));
				cl.add(sub);
			}
			return cl;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return null;
	}

	// ------------------------------------------------------

	/**
	 * @return gets all Submission from data base
	 */
	@SuppressWarnings("unused")
	private ArrayList<StudentEnrollment> getAllStudentEnrollment() {
		String tableName = TABLENAMES[5];
		String searchStatement = "SELECT * FROM " + tableName;
		String fieldName[] = COL[5];
		try {
			ResultSet studentEnrollmentInfo = null;
			ArrayList<StudentEnrollment> cl = new ArrayList<StudentEnrollment>();
			us = conn.prepareStatement(searchStatement);
			studentEnrollmentInfo = us.executeQuery();
			while (studentEnrollmentInfo.next()) {
				StudentEnrollment se = new StudentEnrollment(studentEnrollmentInfo.getString(fieldName[1]),
						studentEnrollmentInfo.getString(fieldName[2]));
				se.setId(studentEnrollmentInfo.getString(fieldName[0]));
				cl.add(se);
			}
			return cl;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return null;
	}

	public String[] getStudents(String s, String courseID, String type) {
		getAllStudentsbyCourseID(s, courseID, type);
		return null;
	}

	public String[] getAllStudentsbyCourseID(String s, String course_id, String type) {
		String tableName = TABLENAMES[5];
		String searchStatement = "SELECT * FROM " + tableName + " WHERE " + COL[5][3] + " = '" + course_id + "' AND "
				+ type + " = '" + s + "'";
		String fieldName[] = COL[5];
		String[] students = null;
		try {
			ResultSet studentEnrollmentInfo = null;
			ArrayList<StudentEnrollment> cl = new ArrayList<StudentEnrollment>();
			us = conn.prepareStatement(searchStatement);
			studentEnrollmentInfo = us.executeQuery();
			StudentEnrollment se = null;
			while (studentEnrollmentInfo.next()) {
				se = new StudentEnrollment(studentEnrollmentInfo.getString(fieldName[1]),
						studentEnrollmentInfo.getString(fieldName[2]));
				se.setId(studentEnrollmentInfo.getString(fieldName[0]));
				cl.add(se);
			}
			students = new String[cl.size()];
			for (int i = 0; i < students.length; i++) {
				students[i] = cl.get(i).getstudent_id();
			}
			return students;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return null;
	}

	// ------------------------------------------------------

	/**
	 * @param user
	 *            : userId
	 * @param pass
	 *            : password
	 * @return : <0 if student and valid>, < 1 if prof and valid>, < -1 invalid>
	 */
	public int validateUserPass(String user, String pass) {
		User users = searchUserbyID(user);

		if (users == null) {
			return -1;
		} else {
			String encryptedPassword = users.getPassword();
			if (passwordEncryptor.checkPassword(pass, encryptedPassword)) {
				if (users.getType().equals("p") || users.getType().equals("P"))
					return 1;
				else if (users.getType().equals("s") || users.getType().equals("S"))
					return 0;
			}
		}

		return -1;
	}

	/**
	 * adds some users to data base
	 */
	public void fillUp() {
		BufferedReader br = null;
		String line;
		String[] fields;
		String inputFile = "users.txt";
		// ArrayList<User> users = new ArrayList<User>();
		try {
			br = new BufferedReader(new FileReader(inputFile));

			while ((line = br.readLine()) != null) {
				fields = line.split(";");
				User ms = new User(fields[0], fields[1], fields[2], fields[3], fields[4]);
				addUser(ms);
			}
		} catch (Exception e) {

		} finally {
			try {
				br.close();
			} catch (IOException e) {
				System.out.println(e);
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DBHelper help = new DBHelper();
		help.createTables();
		// help.createAssignmentTable();
		help.fillUp();
	}

}
