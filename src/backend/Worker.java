package backend;

import java.io.*;
import java.net.Socket;
import shared.*;
import java.util.ArrayList;
import java.util.Iterator;

public class Worker implements Runnable {
    DBHelper database;
    FileHelper fileHelper;

    private Socket workerSocket;

    private BufferedReader socketInput;
    private PrintWriter socketOutput;

    private InputStream is;
    private ObjectInputStream objin;
    private OutputStream os;
    private ObjectOutputStream objout;

    public Worker(Socket inSocket, DBHelper db, FileHelper fh) {
        this.workerSocket = inSocket;
        try{
            this.database = db;
            this.fileHelper = fh;
            socketInput = new BufferedReader(new InputStreamReader(workerSocket.getInputStream()));
            socketOutput = new PrintWriter(workerSocket.getOutputStream(), true);
            setOutputStream();
            setInputStream();
        } catch(IOException e){
            e.printStackTrace();
        }
    }


    @Override
    public void run() {
        while (true) {
            String command;
            String[] inputarray;
            try {
                command = socketInput.readLine();
                if(command.contains("CHECK")){
                    inputarray = command.split(" ");
                    System.out.println("User: "+inputarray[0]+" Pass: "+inputarray[1]);
                    int val = database.validateUserPass(inputarray[0],inputarray[1]);
                    socketOutput.println(val);
                    socketOutput.flush();
                }
                else if(command.contains("GET_COURSES")){
                    inputarray = command.split(" ");
                    System.out.println("Prof ID: "+inputarray[0]);
                    ArrayList <Course> val = database.getCourses(inputarray[0]);
                    sendCoursesToClient(val);
                }
                else if(command.contains("UPLOADING_ASSIGN")){
                    System.out.println("in dl assign");
                    downloadAssignment();
                }
                else if(command.contains("ADD_COURSE")){
                    System.out.println("in add course");
                    addCourse();
                }
                else if(command.contains("GET_STUDENT")){
                    System.out.println("in get student");
                    inputarray = command.split(" ");
                    System.out.println("ID: " + inputarray[0]);
                    User u = database.searchUserbyID(inputarray[0]);
                    sendUsertoClient(u);
                }
                else if(command.contains("CHANGE_VISIBLE")){
                    System.out.println("in change visible");
                    inputarray = command.split(" ");
                    database.changeVisibility(inputarray[0],inputarray[1]);
                }

            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
                System.out.println("error in thread");
            }
        }
    }

    public void addCourse(){
        Course in = null;
        try {
            in = (Course) objin.readObject();
            if(in != null){
                database.addCourse(in);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.err.println("Class not found exception caught.");
        }catch(Exception e){
            System.err.println("Cant get course");
        }
        socketOutput.println("success in addCourse");
    }

    public void downloadAssignment(){
        byte[] in = null;
        try {
            in = (byte[]) objin.readObject();
            File newFile = new File("C:\\Users\\Chan\\Desktop\\" + "assign1" + ".txt");
            if(! newFile.exists())
                newFile.createNewFile();
            FileOutputStream writer = new FileOutputStream(newFile);
            BufferedOutputStream bos = new BufferedOutputStream(writer);
            bos.write(in);
            bos.close();
            System.out.println("success dl");
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.err.println("Class not found exception caught.");
        }catch(Exception e){
            System.err.println("Cant get file");
        }
    }

    public void setOutputStream(){
        try {
            os = workerSocket.getOutputStream();
            objout = new ObjectOutputStream(os);
        } catch (IOException e){
            e.printStackTrace();
            System.out.println("Error in output stream.");
        }
    }

    public void setInputStream(){
        try {
            is = workerSocket.getInputStream();
            objin = new ObjectInputStream(is);
        } catch (IOException e){
            e.printStackTrace();
            System.out.println("Error setting input stream");
        }
    }

    public void sendUsertoClient(User u){
        try {
            socketOutput.println("Sending user to client");
            objout.writeObject(u);
            objout.flush();
        } catch (IOException e){
            e.printStackTrace();
            System.out.println("Error sending courses.");
        }
        socketOutput.println("Done sending user to client");
    }

    public void sendCoursesToClient(ArrayList <Course> c){
        try {
            socketOutput.println("Sending courses to client");
            objout.writeObject(c);
            objout.flush();
        } catch (IOException e){
            e.printStackTrace();
            System.out.println("Error sending courses.");
        }
        socketOutput.println("Done sending courses to client");
    }

    public static void main(String[] args){
        ArrayList<String>s=new ArrayList<>();
        s.add("aa");
        s.add("vv");
        System.out.println(s.size());
    }

}
