/**
 * 
 */
package backend;

/**
 * @author ArjunS
 *
 */
interface Constants {
	final static String DRIVER = "com.mysql.jdbc.Driver";
	final static String PORT = "3306"; // for testing
	final static String URL = "jdbc:mysql://localhost:" + PORT;
	final static String USER = "root";
	final static String PASSWORD = "";
	final static String DATABASENAME = "Wish2Learn";

	final static String[] TABLENAMES = { "usertable", "AssignmentTable ", "GradeTable ", "coursetable",
			"SubmissinTable ", "StudentEnrollmentTable " }; // 6
	final static String[][] COL = { { "id ", "password ", "email ", "firstname ", "lastname ", "type " }, // 6
			{ "id ", "course_id ", "title ", "path ", "active ", "due_date " }, // 6
			{ "id ", "assign_id ", "student_id ", "course_id ", "assignment_grade " }, // 5
			{ "id ", "prof_id ", "name ", "active " }, // 4
			{ "id ", "assignment_id ", "student_id ", "path ", "title ", "submission_grade ", "comments ",
					"timeStamp " }, // 8
			{ "id ", "studen_id ", "course_id " } // 3
	};

	final static String AUTO = "AUTO_INCREMENT ";// + "= 10000000 "; // start value for default ids # 10,000,000Grade
	final static String AUTO_VAL = "10000000 ";
}