/**
 * 
 */
package shared;

import java.io.Serializable;

/**
 * @author ArjunS
 *
 */
public class Course implements Serializable{
	private int id;
	private int prof_id;
	private String name;
	private int course_id;
	private boolean visible;

	/**
	 * 
	 */
	public Course() {
		super();
	}

	/**
	 * @param prof_int
	 * @param name
	 * @param course_id
	 */
	public Course(int prof_int, String name, int course_id) {
		super();
		this.prof_id = prof_int;
		this.name = name;
		this.course_id = course_id;
	}

	public Course(String prof_intstring, String name, String course_idstring, String visible) {
		super();
		int prof_id = 0, course_id = 0, active = 0;
		active = (visible.equals(true))? 1 : 0;
		boolean success = false;
		try {
			prof_id = Integer.parseInt(prof_intstring);
			course_id = Integer.parseInt(course_idstring);
			success = true;
		} catch (Exception e) {
			success = false;
			System.out.println(e);
			e.printStackTrace();
		} finally {
			if (success) {
				this.prof_id = prof_id;
				this.name = name;
				this.course_id = course_id;
				if(active != 0) {
					this.visible = true;
				}
				else{
					this.visible = false;
				}
			}
		}
	}

	public synchronized boolean isVisible() {
		return visible;
	}

	public synchronized void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return the id
	 */
	public synchronized String getId() {
		return "" + id;
	}

	/**
	 * @return the prof_id
	 */
	public synchronized String getProf_int() {
		return "" + prof_id;
	}

	/**
	 * @return the name
	 */
	public synchronized String getName() {
		return name;
	}

	/**
	 * @return the course_id
	 */
	public synchronized String getCourse_id() {
		return String.valueOf(course_id);
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public synchronized void setId(int id) {
		this.id = id;
	}

	/**
	 */
	public synchronized void setProf_int(int prof_int) {
		this.prof_id = prof_int;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public synchronized void setName(String name) {
		this.name = name;
	}

	/**
	 * @param course_id
	 *            the course_id to set
	 */
	public synchronized void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	public void setId(String idString) {
		int id;
		try {
			id = Integer.parseInt(idString);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}
}
