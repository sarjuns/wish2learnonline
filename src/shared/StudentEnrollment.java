/**
 * 
 */
package shared;

import java.io.Serializable;

/**
 * @author ArjunS
 *
 */
public class StudentEnrollment implements Serializable {
	/**
	 * @param student_id
	 * @param course_id
	 */
	public StudentEnrollment(int student_id, int course_id) {
		super();
		this.student_id = student_id;
		this.course_id = course_id;
	}

	private int id;
	private int student_id;
	private int course_id;

	/**
	 * 
	 */
	public StudentEnrollment() {
		super();
	}

	public StudentEnrollment(String student_idstring, String course_idstring) {
		super();
		int student_id = 0, course_id = 0;
		boolean success = false;
		try {
			student_id = Integer.parseInt(student_idstring);
			course_id = Integer.parseInt(course_idstring);
			success = true;
		} catch (Exception e) {
			success = false;
			System.out.println(e);
			e.printStackTrace();
		} finally {
			if (success) {
				this.student_id = student_id;
				this.course_id = course_id;
			}
		}
	}

	/**
	 * @return the id
	 */
	public synchronized String getId() {
		return "" + id;
	}

	/**
	 * @return the student_id
	 */
	public synchronized String getstudent_id() {
		return "" + student_id;
	}

	/**
	 * @return the course_id
	 */
	public synchronized String getCourse_id() {
		return "" + course_id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public synchronized void setId(String id) {
		try {
			this.id = Integer.parseInt(id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param student_id
	 *            the student_id to set
	 */
	public synchronized void setstudent_id(String student_id) {
		try {
			this.student_id = Integer.parseInt(student_id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param course_id
	 *            the course_id to set
	 */
	public synchronized void setCourse_id(String course_id) {
		try {
			this.course_id = Integer.parseInt(course_id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

}
