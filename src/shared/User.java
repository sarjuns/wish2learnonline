/**
 * 
 */
package shared;

import java.io.Serializable;

/**
 * @author ArjunS
 *
 */
public class User implements Serializable {

	private int id;
	private String password;
	private String email;
	private String firstname;
	private String lastname;
	private String type;

	/**
	 * @param password
	 * @param email
	 * @param firstname
	 * @param lastname
	 * @param type
	 */
	public User(String password, String email, String firstname, String lastname, String type) {
		super();
		this.password = password;
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
		this.type = type;
	}

	/**
	 * 
	 */
	public User() {
		super();
	}

	/**
	 * @return the id
	 */
	public synchronized String getId() {
		return "" + id;
	}

	/**
	 * @return the password
	 */
	public synchronized String getPassword() {
		return password;
	}

	/**
	 * @return the email
	 */
	public synchronized String getEmail() {
		return email;
	}

	/**
	 * @return the firstname
	 */
	public synchronized String getFirstname() {
		return firstname;
	}

	/**
	 * @return the lastname
	 */
	public synchronized String getLastname() {
		return lastname;
	}

	/**
	 * @return the type
	 */
	public synchronized String getType() {
		return type;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public synchronized void setId(String id) {
		try {
			this.id = Integer.parseInt(id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public synchronized void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public synchronized void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @param firstname
	 *            the firstname to set
	 */
	public synchronized void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @param lastname
	 *            the lastname to set
	 */
	public synchronized void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public synchronized void setType(String type) {
		this.type = type;
	}

	public String getIdString() {
		return "" + id;
	}

}
