/**
 * 
 */
package shared;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author ArjunS
 *
 */
public class Assignment implements Serializable {

	private int id;
	private int course_id;
	private String title;
	private String path;
	private int bit;
	private Date due_date;

	/**
	 * @param course_id
	 * @param title
	 * @param bit
	 * @param due_date
	 */
	public Assignment(int course_id, String title, String path, int bit, Date due_date) {
		super();
		this.course_id = course_id;
		this.title = title;
		this.path = path;
		this.bit = bit;
		this.due_date = due_date;
	}

	/**
	 * 
	 */
	public Assignment() {
		super();
	}

	public Assignment(String course_idstring, String title, String path, String bitstring4, String due_datestring5) {
		super();
		int course_id = 0, bit = 0;
		Date due_date = null;
		boolean success = false;
		try {
			course_id = Integer.parseInt(course_idstring);
			bit = Integer.parseInt(bitstring4);
			SimpleDateFormat simpleForm = new SimpleDateFormat("yyyy/MM/dd-hh:mm");
			due_date = simpleForm.parse(due_datestring5);
			success = true;
		} catch (Exception e) {
			success = false;
			System.out.println(e);
			e.printStackTrace();
		} finally {
			if (success) {
				this.course_id = course_id;
				this.title = title;
				this.path = path;
				this.bit = bit;
				this.due_date = due_date;
			}
		}

	}

	/**
	 * @return the id
	 */
	public synchronized String getId() {
		return "" + id;
	}

	/**
	 * @return the course_id
	 */
	public synchronized String getCourse_id() {
		return "" + course_id;
	}

	/**
	 * @return the title
	 */
	public synchronized String getTitle() {
		return title;
	}

	/**
	 * @return the bit
	 */
	public synchronized String getBit() {
		return "" + bit;
	}

	/**
	 * @return the due_date
	 */
	public synchronized String getDue_dateString() {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd-hh:mm"); // rquired format for timestamp
		// System.out.println("Format : " + dateFormatter.format(due_date));
		return dateFormatter.format(due_date);
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public synchronized void setId(String id) {
		try {
			this.id = Integer.parseInt(id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

	}

	/**
	 * @param course_id
	 *            the course_id to set
	 */
	public synchronized void setCourse_id(String course_id) {
		try {
			this.course_id = Integer.parseInt(course_id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public synchronized void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @param bit
	 *            the bit to set
	 */
	public synchronized void setBit(String bit) {
		try {
			this.bit = Integer.parseInt(bit);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param due_date
	 *            the due_date to set
	 */
	public synchronized void setDue_date(String due_date) {
		// format : 2018/04/04-10:30
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd-hh:mm");
		try {
			this.due_date = dateFormatter.parse(due_date);
		} catch (ParseException e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	// /**
	// * @param args
	// */
	// public static void main(String[] args) {
	// // TODO Auto-generated method stub
	// System.out.println("ff");
	// }
}
