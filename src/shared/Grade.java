/**
 * 
 */
package shared;

import java.io.Serializable;

/**
 * @author ArjunS
 *
 */
public class Grade implements Serializable {

	/**
	 * @param assign_id
	 * @param student_id
	 * @param course_id
	 * @param assignment_grade
	 */
	public Grade(int assign_id, int student_id, int course_id, int assignment_grade) {
		super();
		this.assign_id = assign_id;
		this.student_id = student_id;
		this.course_id = course_id;
		this.assignment_grade = assignment_grade;
	}

	private int id;
	private int assign_id;
	private int student_id;
	private int course_id;
	private int assignment_grade;

	/**
	 * 
	 */
	public Grade() {
		super();
	}

	public Grade(String assign_idstring, String student_idstring, String course_idstring,
			String assignment_gradestring) {
		super();
		int assign_id = 0, student_id = 0, course_id = 0, assignment_grade = 0;
		boolean success = false;
		try {
			assign_id = Integer.parseInt(assign_idstring);
			student_id = Integer.parseInt(student_idstring);
			course_id = Integer.parseInt(course_idstring);
			assignment_grade = Integer.parseInt(assignment_gradestring);
			success = true;
		} catch (Exception e) {
			success = false;
			System.out.println(e);
			e.printStackTrace();
		} finally {
			if (success) {
				this.assign_id = assign_id;
				this.student_id = student_id;
				this.course_id = course_id;
				this.assignment_grade = assignment_grade;
			}
		}

	}

	/**
	 * @return the id
	 */
	public synchronized String getId() {
		return "" + id;
	}

	/**
	 * @return the assign_id
	 */
	public synchronized String getAssign_id() {
		return "" + assign_id;
	}

	/**
	 * @return the student_id
	 */
	public synchronized String getStudent_id() {
		return "" + student_id;
	}

	/**
	 * @return the course_id
	 */
	public synchronized String getCourse_id() {
		return "" + course_id;
	}

	/**
	 * @return the assignment_grade
	 */
	public synchronized String getAssignment_grade() {
		return "" + assignment_grade;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public synchronized void setId(String id) {
		try {
			this.id = Integer.parseInt(id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param assign_id
	 *            the assign_id to set
	 */
	public synchronized void setAssign_id(String assign_id) {
		try {
			this.assign_id = Integer.parseInt(assign_id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param student_id
	 *            the student_id to set
	 */
	public synchronized void setStudent_id(String student_id) {
		try {
			this.student_id = Integer.parseInt(student_id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param course_id
	 *            the course_id to set
	 */
	public synchronized void setCourse_id(String course_id) {
		try {
			this.course_id = Integer.parseInt(course_id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param assignment_grade
	 *            the assignment_grade to set
	 */
	public synchronized void setAssignment_grade(String assignment_grade) {
		try {
			this.assignment_grade = Integer.parseInt(assignment_grade);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

}
