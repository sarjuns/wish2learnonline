/**
 * 
 */
package shared;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author ArjunS
 *
 */
public class Submission implements Serializable {

	/**
	 * @param assign_id
	 * @param student_id
	 * @param path
	 * @param title
	 * @param submission_grade
	 * @param comments
	 */
	public Submission(int assign_id, int student_id, String path, String title, int submission_grade, String comments) {
		super();
		this.assign_id = assign_id;
		this.student_id = student_id;
		this.path = path;
		this.title = title;
		this.submission_grade = submission_grade;
		this.comments = comments;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd-hh:mm");
		this.timestamp = new String(dateFormatter.format(new Date()));
	}

	private int id;
	private int assign_id;
	private int student_id;
	private String path;
	private String title;
	private int submission_grade;
	private String comments;
	private String timestamp;

	// format : 2018/04/04-10:30
	/**
	 * 
	 */
	public Submission() {
		super();
	}

	public Submission(String assign_idstring, String student_idstring, String path, String title,
			String submission_gradestring, String comments, String timestamp) {
		super();
		int assign_id = 0, student_id = 0, submission_grade = 0;
		boolean success = false;
		try {
			assign_id = Integer.parseInt(assign_idstring);
			student_id = Integer.parseInt(student_idstring);
			submission_grade = Integer.parseInt(submission_gradestring);

		} catch (Exception e) {
			success = false;
			System.out.println(e);
			e.printStackTrace();
		} finally {
			if (success) {
				this.assign_id = assign_id;
				this.student_id = student_id;
				this.path = path;
				this.title = title;
				this.submission_grade = submission_grade;
				this.comments = comments;
				SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd-hh:mm");
				this.timestamp = new String(dateFormatter.format(new Date()));
			}
		}
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public synchronized void setId(String id) {
		try {
			this.id = Integer.parseInt(id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param assign_id
	 *            the assign_id to set
	 */
	public synchronized void setAssign_id(String assign_id) {
		try {
			this.assign_id = Integer.parseInt(assign_id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param student_id
	 *            the student_id to set
	 */
	public synchronized void setStudent_id(String student_id) {
		try {
			this.student_id = Integer.parseInt(student_id);
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public synchronized void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the id
	 */
	public synchronized String getId() {
		return "" + id;
	}

	/**
	 * @return the assign_id
	 */
	public synchronized String getAssign_id() {
		return "" + assign_id;
	}

	/**
	 * @return the student_id
	 */
	public synchronized String getStudent_id() {
		return "" + student_id;
	}

	/**
	 * @return the path
	 */
	public synchronized String getPath() {
		return path;
	}

	/**
	 * @return the title
	 */
	public synchronized String getTitle() {
		return title;
	}

	/**
	 * @return the submission_grade
	 */
	public synchronized String getSubmission_grade() {
		return "" + submission_grade;
	}

	/**
	 * @return the comments
	 */
	public synchronized String getComments() {
		return comments;
	}

	/**
	 * @return the timestamp
	 */
	public synchronized String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public synchronized void setId(int id) {
		this.id = id;
	}

	/**
	 * @param assign_id
	 *            the assign_id to set
	 */
	public synchronized void setAssign_id(int assign_id) {
		this.assign_id = assign_id;
	}

	/**
	 * @param student_id
	 *            the student_id to set
	 */
	public synchronized void setStudent_id(int student_id) {
		this.student_id = student_id;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public synchronized void setPath(String path) {
		this.path = path;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public synchronized void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @param submission_grade
	 *            the submission_grade to set
	 */
	public synchronized void setSubmission_grade(int submission_grade) {
		this.submission_grade = submission_grade;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public synchronized void setComments(String comments) {
		this.comments = comments;
	}

}
